SHELL := /bin/bash
ifndef PREFIX
  PREFIX = /usr
endif
MOD := adv-core
VERSION=$(shell git describe | rev | cut -d- -f2- | rev | sed -e 's/-/.r/' -e 's/^v//')

default:

.PHONY: clean

.FORCE:

install:
	@mkdir -p $(DESTDIR)/$(PREFIX)/share/adv/mods/$(MOD)/perl
	for pl in  $(wildcard perl/*) ; do \
		install -Dv -m 644 $$pl $(DESTDIR)/$(PREFIX)/share/adv/mods/$(MOD)/$$pl ; \
	done
	@mkdir -p $(DESTDIR)/$(PREFIX)/share/adv/mods/$(MOD)/sql
	for sql in  $(wildcard sql/*) ; do \
		install -Dv -m 644 $$sql $(DESTDIR)/$(PREFIX)/share/adv/mods/$(MOD)/$$sql ; \
	done
	@mkdir -p $(DESTDIR)/$(PREFIX)/share/adv/mods/$(MOD)/hooks
	for hook in  $(wildcard hooks/*) ; do \
		install -Dv -m 755 $$hook $(DESTDIR)/$(PREFIX)/share/adv/mods/$(MOD)/$$hook ; \
	done
	install -v -m 644 config.json $(DESTDIR)/$(PREFIX)/share/adv/mods/$(MOD)/
	install -v -m 644 properties.json $(DESTDIR)/$(PREFIX)/share/adv/mods/$(MOD)/
	@sed -i $(DESTDIR)/$(PREFIX)/share/adv/mods/$(MOD)/properties.json -e "s/[\"]version[\"]: [\"].*[\"]/\"version\": \"$(VERSION)\"/"

uninstall:
	@rm -r $(DESTDIR)/$(PREFIX)/share/adv/mods/$(MOD)

clean:


CLI parser operates *after*:
- parse aliases,                *<--OK*
- language normalization,       *<--OK*
- reduce all tokens to atoms,   #'no idea what i meant with that
- discard ignored words.        *<--OK*

The parser expects *one and only one* **action**, and zero or more **arguments**.
These are defined in the table ``cli_actions``.

Command sucking cycle begins when the player enters a line, if the player's command
queue is empty.

Each token will be placed in its slot, according to its type: *<--OK*

```
> eat apple and banana

action | argument1 | argument2
-------+-----------+-----------
  eat  |  apple    |   banana

```

Note that the parser only cares for syntax and does not check almost anything.
Only the needed number of arguments must be provided, so

```
> eat axe and southwest
```

will also suffice for the parser.

If the parser gets two actions, it throws an "error". and cleans the queue.

If it gets an action that is defined with *exactly* the same number of arguments,
posts it for execution and cleans the queue.

If the action needs more arguments, it may do two things:

- if the action allows it, it can pick an object from the outside; the action   *<--DIFFICULT*
definition should provide where and what to go fish for.
If the search was unsuccessful or the action forbids it, then
- display a message (defined for this specific action and argument), and
leave the queue half-filled as it is.

Next line the user enters, the parser finds the queue alredy filled, so appends
the new tokens and starts again: two actions abort the process, etc.

Integer appended to each argument is saved but ignored for now.

#### Multiple actions

If the queue contains more arguments than needed *and* the action only needs
one argument *and allows it*,
the parser "multiplexes" the command in several 'action arg' forms,
and executes them one by one.

#### Inclusive actions

If the queue contains more arguments than needed and the action needs two
arguments *and allows it*,
the parser "multiplexes" the command in several 'action arg1 arg2' forms
where 'arg1' iterates through the given arguments and 'arg2' is the last one.


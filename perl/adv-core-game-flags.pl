#!/usr/bin/env perl
#TODO: FILE: keep flush_stacks(), discard the rest

require "/usr/share/adv/mods/adv-core/perl/adv-core-lib.pl";

sub raise_flag {
	my $call = shift;
	my $buffer = shift;
	my $run_all = shift;
	$run_all = 'false' unless ($run_all eq 'true');
	my $json = JSON->new->encode($buffer);
	if (logIsEnabled(7)) { logAppend(7, "Raising flag: ".$call.", ".$json.", ".$run_all); }
	dbExecute("select raise_flag('".$call."', '".$json."'::jsonb, ".$run_all.");");
}

sub flush_stacks {
	logPush('flush_stacks');
	my $count = 0;
	while (1) {
		$count = $count + 1;
		logAppend(7, "[Pass $count] Calling resolve_flags()");
		dbExecute('select resolve_flags();');
		my $remaining = dbQueryAtom('select count(ord) from api_calls_stack;');
		logAppend(7, "[Pass $count] Remaining API calls: $remaining");
		last unless ($remaining);
		my @api_calls = @{ dbQuerySingle('select ord from api_calls_stack order by ord;') };
		for my $c (0 .. (@api_calls - 1)) {
			logAppend(7, "Processing call: $api_calls[$c]");
			my @call = @{ dbQueryRow("select * from api_calls_stack where ord = '".$api_calls[$c]."';") };
			my $ord = $call[0];
			my $task = $call[1];
			my $buffer = dbQueryAtom("select jsonb_pretty(buffer) from api_calls_stack where ord = '".$ord."';");
			logAppend(6, "Running api_call $ord ($task) with buffer: ".$buffer);
			# Clean the flag first so in case the eval crashes, this does not end in an infinite loop
			dbExecute("delete from api_calls_stack where ord = ".$ord.";");
			# Do not implement launching scripts yet, only callbacks
			eval "$task(\$buffer);";
		}
		last unless ($count < 50); # Only 50 passes for profilaxis
	}
}

sub flush_pendant {
	logPush('flush_pendant');
  my $player = shift;
  my @pendant = @{ dbQueryRow("select * from pendant_queue where player = '".$player."' limit 1;") };
  if (@pendant) {
    my $p_player = $pendant[0];
    my $p_write = $pendant[1];
    my $p_candidates = $pendant[2];
    #my $p_selected = $pendant[3];
    #my $p_call = $pendant[4];
    #my $p_buffer = $pendant[5];
    #my $p_run_all = $pendant[6];
    my $obj = {
      "player" => $p_player,
      "caller" => getAvatar($p_player),
      "candidates" => JSON::decode_json($p_candidates),
      "message_class" => "menu",
      "message_label" => "select_".$p_write
    };
    test_deliver(JSON->new->encode($obj));
  }
}

1;

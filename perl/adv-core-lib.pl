#!/usr/bin/env perl
#TODO: FILE: needs migration and wrapping

require "/usr/share/adv/mods/adv-core/perl/adv-core-lib-functions.pl";

#TODO: definitions should be placed in a separate file
# Define global paths
$WDIR = $ARGV[0];
$MODNAME = "adv-core" unless ($MODNAME);
$MODDIR = $WDIR."/mods/".$MODNAME;
$OUTPUTFILE = $WDIR . '/output.json';
$INPUTFILE = $WDIR . '/input.json';              $INPUT;
$LOGLEVEL = 5;  # Set it for now, until we read the config
$CONFIGFILE = $WDIR."/config.json";              $CONFIG;
$PROPERTIESFILE = $MODDIR."/properties.json";    $PROPERTIES;
$GAMEPROPERTIESFILE = $WDIR."/properties.json";  $GAMEPROPERTIES;  $DBHOST;

# The init sequence needs these
$WORLDEDIT = $WDIR."/worldedit.json";

#TODO: processing input and output should be done/initialized from a function
# Output to server
writeFile($OUTPUTFILE, qw<[]>);

if ((-e $INPUTFILE) and (-s $INPUTFILE > 5)) {
	my $json = readFile($INPUTFILE);
	$INPUT = JSON::decode_json($json);
}

#
# Set the environment
#

logInit();  # This is perhaps ok
# Global data structure to store logging state
$LOGSTATE = {};


#TODO: should go to a separate file
# Configuration file
$CONFIGFILE = $MODDIR."/config.json" unless (-f $CONFIGFILE);
{
	my $json = readFile($CONFIGFILE);
	$CONFIG = JSON::decode_json($json);
	unless ($CONFIG->{'birth'}) {
		$CONFIG->{'birth'} = time();
		my @ignore = ("update");
		$CONFIG->{'debug-ignore'} = dclone(\@ignore);
		$CONFIGFILE = $WDIR . "/config.json";
		my $json = JSON->new->pretty(1)->encode($CONFIG);
		writeFile($CONFIGFILE, $json);
	}
}

#TODO: these too
# Mod properties
my $modprop = readFile($PROPERTIESFILE);
$PROPERTIES = JSON::decode_json($modprop);

# Game properties
my $gameprop = readFile($GAMEPROPERTIESFILE);
$GAMEPROPERTIES = JSON::decode_json($gameprop);
$DBHOST = $GAMEPROPERTIES->{'dbhost'};

# Read log level from config
$LOGLEVEL = $CONFIG->{'log-level'};

1;

#!/usr/bin/env perl
# +OK+

require $LIBDIR . "log.pl";
logLib("error.pl");

if ( fileExists($ERRORFILE) ) {
	my $json = fileRead($ERRORFILE);
	$ERROR = JSON::decode_json($json);
}

# Check if error.json is empty
sub errorCheck {
	foreach (@{ $ERROR }) {
		logFunction(5, "errorCheck() (false)");
		return 0;
	}
	logFunction(5, "errorCheck() (true)");
	return 1;
}

# Check if error.json is the server's flag error
sub errorCheckFlag {
	foreach (@{ $ERROR }) {
		if ($_->{'class'} eq "server") {
			logFunction(5, "errorCheckFlag() (true)");
			return 1;
		}
	}
	logFunction(5, "errorCheckFlag() (false)");
	return 0;
}

sub errorPush {
	my $player = $PLAYER;
	my $message_type = "error";
	my $message = shift;
	logFunction(5, "errorPush($player, $message_type, $message)");
	my $object = {
		"class"   => "send",
		"player"  => $player,
		"type"    => $message_type,
		"message" => $message
	};

	push @ERRORBUF, $object;
}

# Empty the error file
sub errorEmpty {
	logFunction(5, "errorEmpty()");
	jsonWriteEmptyArray($ERRORFILE);
}

sub errorWrite {
	logFunction(5, "errorWrite()");
	my $json = qq<[]>;
	my $out = JSON::decode_json($json);
	push(@{ $out }, $_) foreach (@ERRORBUF);
	jsonWrite($ERRORFILE, $out);
}

sub errorAppend {
	logFunction(5, "errorAppend()");
	my $json = fileRead($ERRORFILE);
	my $out = JSON::decode_json($json);
	push(@{ $out }, $_) foreach (@ERRORBUF);
	jsonWrite($ERRORFILE, $out);
}

sub errorResolve {
	logFunction(5, "errorResolve()");
	if ( errorCheckFlag() ) {
		errorWrite();
	} elsif ( errorCheck() ) {
		# Do nothing
	} else {
		errorAppend();
	}
}

1;

#!/usr/bin/env perl
#TODO: FILE: check which ones are implemented in pg

require "/usr/share/adv/mods/adv-core/perl/adv-core-lib.pl";

#
# Queries
#

sub isEntity {
	my $id = shift;
	my $entity = dbQueryAtom("select count(id) from everything where id = '".$id."';");
	return 1 if ($entity);
	return 0;
}

sub isInstance {
	my $id = shift;
	return dbQueryAtom("select count(id) from objects where id = '".$id."';");
}

sub isAvatar {
	my $id = shift;
	return dbQueryAtom("select count(avatar) from players where avatar = '".$id."';");
}

sub isPlayerConnected {
	my $id = shift;
	return dbQueryAtom("select connected from players where username = '".$id."';");
}

sub isAvatarConnected {
	my $id = shift;
	return dbQueryAtom("select connected from players where avatar = '".$id."';");
}

# BAD?
sub isModRef {
	my $id = shift;
	my $entry = dbQueryAtom("select id from objects where id = '".$id."';");
	if ($entry) { return 1; } else { return 0; }
}

sub getAvatar {
	my $username = shift;
	return dbQueryAtom("select avatar from players where username = '".$username."';");
}

sub getPlayerName {
	my $username = shift;
	return dbQueryAtom("select name from players where username = '".$username."';");
}

sub getAvatarName {
	my $avatar = shift;
	return dbQueryAtom("select name from players where avatar = '".$avatar."';");
}

sub getAvatarPlayer {
	my $avatar = shift;
	return dbQueryAtom("select username from players where avatar = '".$avatar."';");
}

sub getParentId {
	my $self = shift;
	return dbQueryAtom("select target from links where id = '".$self."' and type = 'parent';");
}

sub getParentSlot {
	my $self = shift;
	return dbQueryAtom("select name from links where id = '".$self."' and type = 'parent';");
}

sub getParentTile {
	my $self = shift;
	return $self if (hasTag($self, "biome"));
	my $parent;
	my $cursor = $self;
	while (1) {
		$parent = getParentId($cursor);
		return $parent if (hasTag($parent, "biome"));
		return 0 unless ($parent);
	}
}

sub getAllParentsRecursive {
	my $arg = shift;
	my @array = @{ dbQuerySingle("select get_all_parents_recursive('".$arg."');") };
	return dclone(\@array);
}

sub putObjectInPlace {
	my $object_id = shift;
	my $parent_id = shift;
	my $slot_name = shift;
	return dbExecute("select put_object_in_place('".$object_id."', '".$parent_id."', '".$slot_name."');");
}

sub getType {
	my $arg = shift;
	return dbQueryAtom("select get_type('".$arg."');");
}

sub getTags {
	my $arg = shift;
	my @array = @{ dbQuerySingle("select get_tags('".$arg."');") };
	return dclone(\@array);
}

sub getElements {
	my $arg = shift;
	my @array = @{ dbQuerySingle("select get_elements('".$arg."');") };
	return dclone(\@array);
}

sub getElementsRecursive {
	my $arg = shift;
	my @array = @{ dbQuerySingle("select get_elements_recursive('".$arg."');") };
	return dclone(\@array);
}

sub getElementsTags {
	my $arg = shift;
	my @array = @{ dbQuerySingle("select get_element_tags('".$arg."');") };
	return dclone(\@array);
}

sub getAllInherited {
	my $arg = shift;
	my @array = @{ dbQuerySingle("select get_all_inherited('".$arg."');") };
	@array = grep !/^$arg$/, @array if (isInstance($arg));
	return dclone(\@array);
}

sub getSlots {
	my $arg = shift;
	my @array = @{ dbQuery("select get_slots('".$arg."');") };
	my @result = ();
	for my $a (0 .. (@array - 1)) {
		my @row = @{ $array[$a] };
		push( @result, dclone(\@row) );
	}
	return dclone(\@result);
}

sub getPlugs {
	my $arg = shift;
	my @array = @{ dbQuerySingle("select get_plugs('".$arg."');") };
	return dclone(\@array);
}

sub getSlices {
	my $arg = shift;
	my @array = @{ dbQuerySingle("select get_slices('".$arg."');") };
	return dclone(\@array);
}

sub getLinked {
	my $self = shift;
	my $linktype = shift;
	my $linkname = shift;
	my @linked = @{ dbQuerySingle("select target from links where id = '".$self."' and type = '".$linktype."' and name = '".$linkname."';") };
	return dclone(\@linked);
}

sub getLinkedByType {
	my $self = shift;
	my $linktype = shift;
	my @linked = @{ dbQuerySingle("select target from links where id = '".$self."' and type = '".$linktype."';") };
	return dclone(\@linked);
}

sub getClasses {
	my $id = shift;
	my $table = dbQueryAtom("select in_table from everything where id = '".$id."';");
	my @query = @{ dbQueryAtom("select classes from ".$table." where id = '".$id."';") };
	@query = grep( !/^$id$/, @query ) if ( isInstance($id) );
	return dclone(\@query);
}

sub getFirstClass {
	my $id = shift;
	my $table = dbQueryAtom("select in_table from everything where id = '".$id."';");
	my @query = @{ dbQueryAtom("select classes from ".$table." where id = '".$id."';") };
	return $query[0];
}

sub getClassesRecursive {
	my $id = shift;
	my @cursor = ($id);
	my @result = ();
	my @row = ();
	my $added;
	while (1) {
		$added = 0;
		push(@result, @cursor);
		foreach my $c (0 .. (@cursor - 1)) {
			my @array = @{ getClasses($cursor[$c]) };
			my $found = @array;
			next if ($found < 1);
			push(@row, @array);
			$added = 1;
		}
		last unless ($added);
		@cursor = ();
		push (@cursor, @row);
		@row = ();
	}
	@result = grep( !/^$id$/, @result ) if ( isInstance($id) );
	return dclone(\@result);
}

sub getAllInSelf {
	my $arg = shift;
	my @array = @{ dbQuerySingle("select get_all_in_self('".$arg."');") };
	return dclone(\@array);
}

sub getAllInSelfRecursive {
	my @cursor = (shift);
	my @result = ();
	my @row = ();
	my $added;
	while (1) {
		$added = 0;
		push(@result, @cursor);
		foreach my $c (0 .. (@cursor - 1)) {
			my @array = @{ getAllInSelf($cursor[$c]) };
			my $found = @array;
			next if ($found < 1);
			push(@row, @array);
			$added = 1;
		}
		last unless ($added);
		@cursor = ();
		push (@cursor, @row);
		@row = ();
	}
	return dclone(\@result);
}

sub getAllInParent {
	my $self = shift;
	return dclone(\@{ dbQuerySingle("select get_all_in_parent('".$self."');") });
}

sub getAllTags {
	my $id = shift;
	my @array = @{ dbQuerySingle("select get_all_tags('".$id."');") };
	my @unique = uniq(@array);
	return dclone(\@unique);
}

sub getClassAttributes {
	my $class = shift;
	my @attributes = @{ $ATTR_INDEX->{'by_class'}->{$class} };
	return dclone(\@attributes);
}

sub getAllInheritedAttributes {
	my $id = shift;
	my @inherited = @{ getAllInherited($id) };
	my @attributes = ();
	foreach my $i (0 .. (@inherited - 1)) {
		my @array = @{ getClassAttributes($inherited[$i]) };
		push(@attributes, @array);
	}
	my @unique = uniq(@attributes);
	return dclone(\@unique);
}

sub getOuterRing {
	my $self = shift;
	my $distance = shift;
	my @ring = ();
	my $cursor = getParentTile($self);
	for my $i (1 .. $distance) {
		$cursor = dbQueryAtom("select target from links where id = '".$cursor."' and type = 'neighbour' and name = 'north_west';");
		return unless ($cursor);
	}
	for my $r (1 .. ($distance * 2)) {
		$cursor = dbQueryAtom("select target from links where id = '".$cursor."' and type = 'neighbour' and name = 'east';");
		return unless ($cursor);
		push(@ring, $cursor);
	}
	for my $r (1 .. ($distance * 2)) {
		$cursor = dbQueryAtom("select target from links where id = '".$cursor."' and type = 'neighbour' and name = 'south';");
		return unless ($cursor);
		push(@ring, $cursor);
	}
	for my $r (1 .. ($distance * 2)) {
		$cursor = dbQueryAtom("select target from links where id = '".$cursor."' and type = 'neighbour' and name = 'west';");
		return unless ($cursor);
		push(@ring, $cursor);
	}
	for my $r (1 .. ($distance * 2)) {
		$cursor = dbQueryAtom("select target from links where id = '".$cursor."' and type = 'neighbour' and name = 'north';");
		return unless ($cursor);
		push(@ring, $cursor);
	}
	return dclone(\@ring);
}

sub getAllTriggers {
	my $target = shift;
	my $task = shift;
	my @inherited = @{ getAllInherited($target) };
	my @triggers = ();
#	print "Digging for triggers with ".$target.", ".$task." into: ".JSON->new->encode(dclone(\@inherited)).")\n";
	foreach my $i (0 .. (@inherited - 1)) {
		my @array = @{ dbQuerySingle("select id from triggers where target = '".$inherited[$i]."' and task = '".$task."';") };
		push(@triggers, @array);
	#	my @tagged = @{ dbQuerySingle("select t.id from triggers t where t.target = '".$inherited[$i]."' and t.tags @> '{".$task."}';") };
	#	push(@triggers, @tagged);
	}
	return dclone(\@triggers);
}

sub triggerHasTag {
	my $trigger = shift;
	my $tag = shift;
	my @array = @{ dbQuerySingle("select unnest(tags) from triggers where id = '".$trigger."';") };
	for my $a (0 .. (@array - 1)) {
		my $t = $array[$a];
		return 1 if ($t =~ /^$tag$/);
	}
	return 0;
}

sub conditionHasTag {
	my $condition = shift;
	my $tag = shift;
	my @array = @{ dbQuerySingle("select unnest(tags) from conditions where id = '".$condition."';") };
	for my $a (0 .. (@array - 1)) {
		my $t = $array[$a];
		return 1 if ($t =~ /^$tag$/);
	}
	return 0;
}

1;

#!/usr/bin/env perl
#TODO: FILE: needs migration and wrapping

#
# This file should be required by all in-game scripts
# (e.g. commands)
#

require "/usr/share/adv/mods/adv-core/perl/adv-core-lib.pl";
#require "/usr/share/adv/mods/adv-core/perl/adv-core-parser.pl";
require "/usr/share/adv/mods/adv-core/perl/adv-core-game-players.pl";
require "/usr/share/adv/mods/adv-core/perl/adv-core-game-queries.pl";
require "/usr/share/adv/mods/adv-core/perl/adv-core-game-checks.pl";
#require "/usr/share/adv/mods/adv-core/perl/adv-core-game-attributes.pl";
#require "/usr/share/adv/mods/adv-core/perl/adv-core-game-objects.pl";
require "/usr/share/adv/mods/adv-core/perl/adv-core-game-world.pl";
#require "/usr/share/adv/mods/adv-core/perl/adv-core-game-conditions.pl";
#require "/usr/share/adv/mods/adv-core/perl/adv-core-game-triggers.pl";
#require "/usr/share/adv/mods/adv-core/perl/adv-core-game-modifiers.pl";
require "/usr/share/adv/mods/adv-core/perl/adv-core-game-flags.pl";
#require "/usr/share/adv/mods/adv-core/perl/adv-core-game-words.pl";

#
# Autoload all queries, tests, flags, modifiers, and whatever the mods provide
#
#TODO: all initialization like this should be done once and for all in a unique file

my @AUTOLOAD = ();
foreach (@{ $GAMEPROPERTIES->{'mods'} }) {
	my $mod = $_;
	my $dir = $WDIR."/mods/".$mod."/autoload";
	if ( -d $dir ) {
		local $/;
		opendir my $dh, $dir;
		my @files = readdir $dh;
		closedir $dh;
		for my $i (0 .. (@files - 1)) {
			push(@AUTOLOAD, $dir.'/'.$files[$i])
				if ($files[$i] =~ /.*\.pl/);
		} # for my $i (0 .. (@files - 1)) ...
	} # if ( -d $dir ) ...
} # foreach (@{ $GAMEPROPERTIES->{'mods'} }) ...

writeFile($WDIR."/autoload.pl", '');
local $/;
open my $afh, '>>', $WDIR."/autoload.pl";
foreach my $a (0 .. (@AUTOLOAD - 1)) {
	my $string = 'require "'.$AUTOLOAD[$a].'";'."\n";
	print $afh $string;
}
print $afh qw<1;>;
close $afh;
require $WDIR."/autoload.pl";

#
# Collect input
#

#TODO: this goes to the same function that defined $OUTPUT etc
$player = $INPUT->{'player'};
$inbuffile = $ARGV[1];
$inbuf;
if (-f $inbuffile) {
	my $json = readFile($inbuffile);
	$inbuf = JSON::decode_json($json);
}

1;

#!/usr/bin/env perl
#TODO: FILE: needs to be implemented in pg

require "/usr/share/adv/mods/adv-core/perl/adv-core-lib.pl";

#
# Checks
#

sub hasTag {
	my $id = shift;
	my $string = shift;
	my @tags = @{ getAllTags($id) };
	foreach my $t (0 .. (@tags - 1)) {
		return 1 if ($tags[$t] eq $string);
	}
	return 0;
}

sub hasOwnTag {
	my $id = shift;
	my $string = shift;
	my @classes;
	if (isInstance($id)) {
		@classes = @{ getClasses($id) };
	} elsif (isEntity($id)) {
		@classes = ($id);
	} else {
		return 0;
	}
	my $valid = @classes;
	return 0 unless (@classes);
	my @tags;
	foreach my $c (0 .. (@classes - 1)) {
		my $table = dbQueryAtom("select in_table from everything where id = '".$classes[$c]."';");
		push(@tags, @{ dbQueryAtom("select tags from ".$table." where id = '".$classes[$c]."';") });
	}
	foreach my $t (0 .. (@tags - 1)) {
		return 1 if ($tags[$t] eq $string);
	}
	return 0;
}

sub hasClass {
	my $id = shift;
	my $string = shift;
	my @classes = @{ getClassesRecursive($id) };
	foreach my $c (0 .. (@classes - 1)) {
		return 1 if ($classes[$c] eq $string);
	}
	return 0;
}

sub hasOwnClass {
	my $id = shift;
	my $string = shift;
	my @classes;
	if (isInstance($id)) {
		@classes = @{ getClasses($id) };
	} else {
		@classes = ($id);
	}
	foreach my $c (0 .. (@classes - 1)) {
		return 1 if ($classes[$c] eq $string);
	}
	return 0;
}

1;

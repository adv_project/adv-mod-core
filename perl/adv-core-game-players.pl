#!/usr/bin/env perl
#TODO: FILE: needs migration

require "/usr/share/adv/mods/adv-core/perl/adv-core-lib.pl";

# TODO: this entire file is a mess

#
# Player management
#

sub checkOwner {
	return ($GAMEPROPERTIES->{'owner'} eq $INPUT->{'player'});
}

sub isPlayerRegistered {
	my $player = shift;
	return 0 unless (dbQueryAtom("select username from players where username = '$player';"));
	return 1;
}

sub initializePlayer {
	my $player = shift;
	my $as_owner = shift;
	my $phase;
	if ($as_owner) {
		$phase = 'world';
	} else {
		$phase = 'character';
	}
	my $path = $WDIR."/players/".$player.".json";
	my $json = '{
		"race" : "",
		"gender" : "",
		"class" : "",
		"name " : "",
		"birth" : "'. time().'",
		"phase" : "'.$phase.'",
		"aliases" : {
			"n" : "north",
			"ne" : "north_east",
			"e" : "east",
			"se" : "south_east",
			"s" : "south",
			"sw" : "south_west",
			"w" : "west",
			"nw" : "north_west"
		},
		"pointers" : {},
		"given-names" : {}
	}';
	writeFile($path, $json);
}

sub registerPlayer {
	my $player = shift;
	my $as_owner = shift;
  my $phase = 'character';
	if ($as_owner) {
    $phase = 'world';
		callSend($player, "info", "Welcome $player, you are registered as owner");
	} else {
		callSend($player, "info", "Welcome $player, you are registered now");
	}
	dbExecute("insert into everything values ('".$player."', 'player', 'players');");
	dbExecute("insert into players (username, game_phase, language) values ('".$player."', '".$phase."', 'en');");
#	initializePlayer($player, $as_owner);
}

sub setPlayerConnected {
	my $player = shift;
	dbExecute("update players set connected = true, last_connected = ".time()." where username = '".$player."';");
}

sub setPlayerDisconnected {
	my $player = shift;
	dbExecute("update players set connected = false, last_connected = ".time()." where username = '".$player."';");
}

sub getPlayerPhase {
	my $player = shift;
  my $phase = dbQueryAtom("select game_phase from players where username = '".$player."';");
	return $phase;
}
sub setPlayerPhase {
	my $player = shift;
	my $phase = shift;
  dbExecute("update players set game_phase = '".$phase."' where username = '".$player."';");
}

1;

#!/usr/bin/env perl
#TODO: FILE: keep it as is except for some functions

use JSON;
use DBI;
use List::Util 'shuffle';
use List::MoreUtils qw(any uniq);
use Time::HiRes qw(time);
use Storable qw(dclone);
use POSIX qw(strftime);
use feature qw(switch);
no warnings;

sub readFile {
	local $/;
	my $path = shift;
	return undef unless (-f $path);
	open my $fh, '<', $path;
	my $output = <$fh>;
	close $fh;
#	if (logIsEnabled(7)) {
#		logMsg(7, join "->", @{ $LOGSTATE->{'trace'} });
#		logMsg(7, "Reading file: ".$path, $output, "io");
#	}
	return $output;
}

sub writeFile {
	local $/;
	my $path = shift;
	my $content = shift;
	open my $fh, '>', $path;
	print $fh $content;
	close $fh;
#	if (logIsEnabled(7)) {
#		logMsg(7, join "->", @{ $LOGSTATE->{'trace'} });
#		logMsg(7, "Writing file: ".$path, $content, "io");
#	}
}

sub appendFile {
	local $/;
	my $path = shift;
	my $content = shift;
	open my $fh, '>>', $path;
	print $fh $content;
	close $fh;
#	if (logIsEnabled(7)) {
#		logMsg(7, join "->", @{ $LOGSTATE->{'trace'} });
#		logMsg(7, "Appending to file: ".$path, $content, "io");
#	}
}

#
# Database access
#

# Create database schema for this game
# dbCreateSchema()
sub dbCreateSchema {
	local $/;
	my $schema = $GAMEPROPERTIES->{'name'};
	my $commands = 'create schema '.$schema.';';
	my $dsn = "DBI:Pg:dbname=adv;host=".$DBHOST.";port=5432";
	if (logIsEnabled(7)) {
#		logMsg(7, join "->", @{ $LOGSTATE->{'trace'} });
		logMsg(7, "Connecting to database: '".$commands."'");
	}
	my $dbh = DBI->connect($dsn, "adv", "", { RaiseError => 0 });
	my $sth = $dbh->prepare( $commands );
	my $rv = $sth->execute();
	print $DBI::errstr unless ($rv >= 0);
	$sth->finish();
	$dbh->disconnect();
}

# Destroy database schema
# dbDestroySchema()
sub dbDestroySchema {
	local $/;
	my $schema = $GAMEPROPERTIES->{'name'};
	my $commands = 'drop schema '.$schema.' cascade;';
	my $dsn = "DBI:Pg:dbname=adv;host=".$DBHOST.";port=5432";
	if (logIsEnabled(7)) {
#		logMsg(7, join "->", @{ $LOGSTATE->{'trace'} });
		logMsg(7, "Connecting to database: '".$commands."'");
	}
	my $dbh = DBI->connect($dsn, "adv", "", { RaiseError => 0 });
	my $sth = $dbh->prepare( $commands );
	my $rv = $sth->execute();
	print $DBI::errstr unless ($rv >= 0);
	$sth->finish();
	$dbh->disconnect();
}

# Execute a given command in the database
# dbExecute(commands)
sub dbExecute {
	local $/;
	my $schema = $GAMEPROPERTIES->{'name'};
	my $commands = shift;
	my $dsn = "DBI:Pg:dbname=adv;host=".$DBHOST.";port=5432";
	if (logIsEnabled(7)) {
#		logMsg(7, join "->", @{ $LOGSTATE->{'trace'} });
		logMsg(7, "Connecting to database: '".$commands."'");
	}
	my $dbh = DBI->connect($dsn, "adv", "", { RaiseError => 0 });
	my $sth = $dbh->prepare( 'set search_path to '.$schema.'; '.$commands );
	my $rv = $sth->execute();
	$sth->finish();
	$dbh->disconnect();
}

# Read commands from a file and execute them it the database
# dbExecuteFile(path)
sub dbExecuteFile {
	my $sqlfile = shift;
	my $commands = readFile($sqlfile);
	dbExecute($commands);
}

# Query the database and return the result
# dbQuery(query)
# Outputs a 2-dimensional array, in the form:
# array[row][column]
sub dbQuery {
	local $/;
	my $schema = $GAMEPROPERTIES->{'name'};
	my $commands = shift;
	my $dsn = "DBI:Pg:dbname=adv;host=".$DBHOST.";port=5432";
	if (logIsEnabled(7)) {
#		logMsg(7, join "->", @{ $LOGSTATE->{'trace'} });
		logMsg(7, "Connecting to database: '".$commands."'");
	}
	my $dbh = DBI->connect($dsn, "adv", "", { RaiseError => 0 })
   or die $DBI::errstr;
	my $sth = $dbh->prepare( 'set search_path to '.$schema.'; '.$commands );
	my $rv = $sth->execute() or die $DBI::errstr;
	print $DBI::errstr unless ($rv >= 0);
	my @result = ();
	while(my @row = $sth->fetchrow_array()) {
    my @columns = ();
  	for my $r (0 .. (@row - 1)) {
	  	push(@columns, $row[$r]);
  	}
		push(@result, dclone(\@columns));
	}
	$sth->finish();
	$dbh->disconnect();
	return dclone(\@result);
}

# Query the database and return only one array,
# useful to queries that return only one colums.
# dbQuerySingle(query)
sub dbQueryRow {
	local $/;
	my $schema = $GAMEPROPERTIES->{'name'};
	my $commands = shift;
	my $dsn = "DBI:Pg:dbname=adv;host=".$DBHOST.";port=5432";
	if (logIsEnabled(7)) {
#		logMsg(7, join "->", @{ $LOGSTATE->{'trace'} });
		logMsg(7, "Connecting to database: '".$commands."'");
	}
	my $dbh = DBI->connect($dsn, "adv", "", { RaiseError => 0 })
   or die $DBI::errstr;
	my $sth = $dbh->prepare( 'set search_path to '.$schema.'; '.$commands );
	my $rv = $sth->execute() or die $DBI::errstr;
	print $DBI::errstr unless ($rv >= 0);
	my @result = ();
	my @row = $sth->fetchrow_array();
	for my $r (0 .. (@row - 1)) {
		push(@result, $row[$r]);
	}
	$sth->finish();
	$dbh->disconnect();
	return dclone(\@result);
}

# Query the database and return only one array,
# useful to queries that return only one colums.
# dbQuerySingle(query)
sub dbQuerySingle {
	local $/;
	my $schema = $GAMEPROPERTIES->{'name'};
	my $commands = shift;
	my $dsn = "DBI:Pg:dbname=adv;host=".$DBHOST.";port=5432";
	if (logIsEnabled(7)) {
#		logMsg(7, join "->", @{ $LOGSTATE->{'trace'} });
		logMsg(7, "Connecting to database: '".$commands."'");
	}
	my $dbh = DBI->connect($dsn, "adv", "", { RaiseError => 1 })
   or die $DBI::errstr;
	my $sth = $dbh->prepare( 'set search_path to '.$schema.'; '.$commands );
	my $rv = $sth->execute() or die $DBI::errstr;
	print $DBI::errstr unless ($rv >= 0);
	my $result = JSON::decode_json(qw<[]>);
	while(my @row = $sth->fetchrow_array()) {
		push @{ $result }, @row[0];
	}
	$sth->finish();
	$dbh->disconnect();
	return dclone($result);
}

# Query the database and return only one element.
# dbQueryAtom(query)
sub dbQueryAtom {
	local $/;
	my $schema = $GAMEPROPERTIES->{'name'};
	my $commands = shift;
	if (logIsEnabled(7)) { logMsg(7, "Connecting to database: '".$commands."'"); }
	my $dsn = "DBI:Pg:dbname=adv;host=".$DBHOST.";port=5432";
	my $dbh = DBI->connect($dsn, "adv", "", { RaiseError => 1 })
   or die $DBI::errstr;
	if (logIsEnabled(7)) { logMsg(7, "dbh->prepare()"); }
	my $sth = $dbh->prepare( 'set search_path to '.$schema.'; '.$commands );
	if (logIsEnabled(7)) { logMsg(7, "sth->execute()"); }
	my $rv = $sth->execute() or die $DBI::errstr;
	print $DBI::errstr unless ($rv >= 0);
	if (logIsEnabled(7)) { logMsg(7, "sth->fetchrow_array()"); }
	my @row = $sth->fetchrow_array();
	if (logIsEnabled(7)) { logMsg(7, "sth->finish()"); }
	$sth->finish();
	if (logIsEnabled(7)) { logMsg(7, "sth->disconnect()"); }
	$dbh->disconnect();
	return $row[0];
}

sub dbLog {
	my $loglevel = shift;
	my $logclass = shift;
	my $logmessage = shift;
	dbExecute("log_event('".$loglevel."', '".$logclass."', '".$logmessage."');");
}

#
# Helper db functions
#

sub newId {
	return dbQueryAtom('select new_id(16);');
}

sub newPath {
	my $suffix = shift;
	my $newpath;
	while (1) {
		$newpath = $WDIR.'/'.newId().$suffix;
		last unless (-f $newpath);
	}
	return $newpath;
}

sub getNewId {
	return dbQueryAtom('select make();');
}

#
# API calls
#

# Pushes an API call into output array (effectively writes
# it to file)
sub outputPush {
	my $object = shift;
	my $json = readFile($OUTPUTFILE);
	my $out = JSON::decode_json($json);
	push @{ $out }, $object;
	$json = JSON->new->pretty(1)->encode( dclone($out) );
	writeFile($OUTPUTFILE, $json);
}

sub callSend {
	my $player = shift;
	my $message_type = shift;
	my $message = shift;
	my $object = {
		"class"   => "send",
		"player"  => $player,
		"type"    => $message_type,
		"message" => $message
	};
	outputPush($object);
}

sub callSendTagged {
	my $player = shift;
	my $message_type = shift;
	my $message = shift;
	my $tag = shift;
	my $object = {
		"class"   => "send",
		"player"  => $player,
		"type"    => $message_type,
		"message" => $message,
		"tag" => $tag
	};
	outputPush($object);
}

sub callKick {
	my $player = shift;
	my $object = {
		"class"   => "kick",
		"player"  => $player
	};
	outputPush($object);
}

sub callLog {
	my $loglevel = int(shift);
	my $message = shift;
	my $object = {
		"class"   => "log",
		"log-level"  => $loglevel,
		"message" => $message
	};
	outputPush($object);
}

sub callRun {
	my $path = shift;
	my $input = shift;
	my $object = {
		"class"   => "run",
		"path"  => $path,
		"input" => $input
	};
	outputPush($object);
}

sub callPause {
	my $object = {
		"class"   => "pause"
	};
	outputPush($object);
}

sub callResume {
	my $object = {
		"class"   => "resume"
	};
	outputPush($object);
}

sub callSendPlain   { callSend(shift, "plain",   shift); }
sub callSendInfo    { callSend(shift, "info",    shift); }
sub callSendNotice  { callSend(shift, "notice",  shift); }
sub callSendWarning { callSend(shift, "warning", shift); }
sub callSendError   { callSend(shift, "error",   shift); }

sub callLogDebug    { callLog(7, shift); }
sub callLogInfo     { callLog(6, shift); }
sub callLogNotice   { callLog(5, shift); }
sub callLogWarn     { callLog(4, shift); }
sub callLogError    { callLog(3, shift); }
sub callLogCrit     { callLog(2, shift); }
sub callLogAlert    { callLog(1, shift); }
sub callLogEmerg    { callLog(0, shift); }

#
# Server and debug logs
#

sub logIsEnabled {
	my $loglevel = shift;
	return 0 if ( int($loglevel) > int($LOGLEVEL) );
	return 1;
}

sub logAppend {
	my $loglevel = shift;
	my $message = shift;
	my $data = shift;
	my $loglabel = shift;
#	my $timestamp = strftime "%Y-%m-%d-%H:%M:%s", localtime;
	my $t = time;
	my $timestamp = strftime "%Y-%m-%d %H:%M:%S", localtime $t;
	$timestamp .= sprintf ".%03d", ($t-int($t))*1000;
	my @logtags = (
		'EMERG',
		'ALERT',
		'CRIT',
		'ERROR',
		'WARN',
		'NOTICE',
		'INFO',
		'DEBUG'
	);
	my $string = $timestamp." [".$logtags[$loglevel]."] ".$message;
	$loglabel = 'messages' unless ($loglabel);
	$logfile = $LOGSTATE->{'logdir'}.'/'.$loglabel.'.log';
	appendFile($logfile, $string."\n");
	return 1 unless ($data);
	appendFile($logfile, $data."\n");
	return 1;
}

sub logMsg {
	my $loglevel = shift;
	return unless ( logIsEnabled($loglevel) );
	my $message = shift;
	my $data = shift;
	my $loglabel = shift;
	if (( int($loglevel) > 5 ) and ($CONFIG)) {
		logAppend($loglevel, $message, $data, $loglabel) unless (logIsIgnored($LOGSTATE->{'caller'}));
	}
	$message = $message." (".$loglabel.".log)" if ($loglabel);
	callLog($loglevel, $message);
}

sub logIsIgnored {
	my $caller = shift;
	my @ignored = @{ $CONFIG->{'debug-ignore'} };
#	print join ", ", @ignored."\n";
	for my $i (0 .. (@ignored - 1)) {
		my $string = $ignored[$i];
		if ($caller =~ /$string/) {
#			print "$caller matches $string, ignoring\n";
			return 1;
		}
	}
#	print "Allowing $caller to log\n";
	return 0;
}

sub logInit {
	callLogNotice("#===== Adv core starting at ".time()."======");
	return 1 unless ($CONFIG);
	my $caller = shift;
	mkdir($WDIR.'/logs/') unless (-d $WDIR.'/logs');
	$LOGSTATE->{'timestamp'} = strftime "%Y-%m-%d-%H-%M-%S", localtime;
	$LOGSTATE->{'logdir'} = $WDIR.'/logs/'.$caller.'-'.$LOGSTATE->{'timestamp'};
	my $symlink = $WDIR.'/logs/'.$caller.'-last';
	my @array = ($caller);
	$LOGSTATE->{'trace'} = dclone(\@array);
	$LOGSTATE->{'caller'} = $caller;
	return 1 unless (( int($LOGLEVEL) > 5 ) and ( ! logIsIgnored($caller) ));
	mkdir($LOGSTATE->{'logdir'});
	unlink($symlink);
	symlink($LOGSTATE->{'logdir'}, $symlink);
	logAppend(5, "Adv core starting at ".time());
	if (logIsEnabled(7)) {
		logMsg(7, "input.json", JSON->new->pretty(1)->encode($INPUT), "input");
	}
	return 1;	
}

sub logPush {
	my @array = @{ $LOGSTATE->{'trace'} };
	push @array, shift;
	$LOGSTATE->{'trace'} = dclone(\@array);
	logMsg(7, "=== ENTER ".join("->", @array)." ====================");
}

sub logPop {
	my @array = @{ $LOGSTATE->{'trace'} };
	pop @array, shift;
	logMsg(7, "--- Back to ".join("->", @array)." --------");
	$LOGSTATE->{'trace'} = dclone(\@array);
}

1;

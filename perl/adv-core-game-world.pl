#!/usr/bin/env perl
#TODO: FILE: already implemented in pg

require "/usr/share/adv/mods/adv-core/perl/adv-core-lib.pl";

#
# Manipulating world parameters
#

sub getTimeData {
	my $world = shift;
	my @array = @{ dbQueryRow("select year, day_of_year, tick_of_day, days_per_year, ticks_per_day, steps_per_tick, world_tilt from worlds where id = '".$world."';") };
	my $year = $array[0];
	my $day_of_year = $array[1];
	my $tick_of_day = $array[2];
	my $days_per_year = $array[3];
	my $ticks_per_day = $array[4];
	my $steps_per_tick = $array[5];
	my $world_tilt = $array[6];
	my $current_tick = (($year-1)*$days_per_year + ($day_of_year-1))*$ticks_per_day + ($tick_of_day-1);
	my $tick_of_year = ($current_tick % ($ticks_per_day*$days_per_year));
#	my $year_round_position = 2 * $PI * $time_t->{'tick_of_year'} / ( $time_t->{'ticks_per_day'}*$time_t->{'days_per_year'} );
	my $time_t = {
		 "year" => $year,
		 "day_of_year" => $day_of_year,
		 "tick_of_day" => $tick_of_day,
		 "days_per_year" => $days_per_year,
		 "ticks_per_day" => $ticks_per_day,
		 "steps_per_tick" => $steps_per_tick,
		 "world_tilt" => $world_tilt,
		 "current_tick" => $current_tick,
		 "tick_of_year" => $tick_of_year
	};
	return $time_t;
}

sub advanceTime {
	my $world = shift;
	my $interval = shift;
	# Rettrieve time data
	my $time_t = getTimeData($world);
	my $year = $time_t->{'year'};
	my $day_of_year = $time_t->{'day_of_year'};
	my $tick_of_day = $time_t->{'tick_of_day'};
	my $days_per_year = $time_t->{'days_per_year'};
	my $ticks_per_day = $time_t->{'ticks_per_day'};
	# Advance clock
	$tick_of_day = $tick_of_day + $interval;
	if ($tick_of_day > $ticks_per_day) {
		$day_of_year = $day_of_year + 1;
		$tick_of_day = 1;
	}
	if ($day_of_year > $days_per_year) {
		$year = $year + 1;
		$day_of_year = 1;
	}
	# Update table worlds
	dbExecute("update worlds set year = ".$year.", day_of_year = ".$day_of_year.", tick_of_day = ".$tick_of_day." where id = '".$world."';");
}

1;

/* Attributes index, used to reference attributes by class and viceversa */

CREATE TABLE attributes_index (
    id         VARCHAR  REFERENCES everything ON DELETE CASCADE,
    attribute  VARCHAR  REFERENCES everything ON DELETE CASCADE
);

/* Boundaries, used as attribute value ranges */

CREATE TABLE boundaries (
    id     VARCHAR  REFERENCES everything ON DELETE CASCADE,
    label  VARCHAR,
	 value  NUMERIC
);

/* Traces of estimated attributes */

CREATE TABLE estimated_values (
  id          VARCHAR  REFERENCES everything ON DELETE CASCADE,
	attribute   VARCHAR,
	value       NUMERIC
);


/* Reboot the CLI queue for this player */
CREATE OR REPLACE FUNCTION clear_cli_queue(
  f_player VARCHAR
)
RETURNS VOID AS $$
  BEGIN
    DELETE FROM cli_queue q
    WHERE       q.player = f_player;
  END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION resolve_user_input(
  input JSONB
)
RETURNS VOID AS $$

  DECLARE
    token               VARCHAR;
    player_game_phase   VARCHAR;
    actions_in_queue    INTEGER;
    arguments_in_queue  INTEGER;
    arguments           VARCHAR[];
    selected_action     VARCHAR;
    i                   INTEGER;
    action_record       RECORD;
    success             BOOLEAN       DEFAULT FALSE;
    overflow            BOOLEAN       DEFAULT TRUE;
    hint                VARCHAR[];
    buf                 JSONB;
    caller              VARCHAR;

  BEGIN
/*TODO: cli menu: this function should not run at all if returning from a menu */
    FOR token IN
      SELECT  *
      FROM    jsonb_array_elements_text(input->'action')
    LOOP
      perform resolve_cli_token(token, input->>'player'::VARCHAR);
    END LOOP;
    
    SELECT  p.game_phase
    FROM    players p
    WHERE   p.username = input->>'player'
    INTO    player_game_phase;

    SELECT  count(*)
    FROM    cli_queue q
    WHERE   q.player = input->>'player'
    AND     q.is_action = TRUE
    INTO    actions_in_queue;

    IF actions_in_queue > 1
    THEN
      PERFORM push_api_call('test_deliver', jsonb_build_object(
        'message_class'::TEXT,  'plain'::TEXT,
        'message_label'::TEXT,  'too_many_actions'::TEXT,
        'recipient'::TEXT,      input->>'player'::TEXT
      ));

      DELETE FROM cli_queue
      WHERE       player = input->>'player';
    ELSIF actions_in_queue < 1
    THEN
      PERFORM push_api_call('test_deliver', jsonb_build_object(
        'message_class'::TEXT,  'plain'::TEXT,
        'message_label'::TEXT,  'no_actions_provided'::TEXT,
        'recipient'::TEXT,      input->>'player'::TEXT
      ));

      DELETE FROM cli_queue
      WHERE       player = input->>'player';
    ELSE
      SELECT  count(*)
      FROM    cli_queue q
      WHERE   q.player = input->>'player'
      AND     q.is_action = FALSE
      INTO    arguments_in_queue;
    
      SELECT  q.token
      FROM    cli_queue q,
              cli_actions a
      WHERE   q.is_action = TRUE
      AND     q.player = input->>'player'
      AND     q.token = a.action
      AND     a.game_phase = player_game_phase
      INTO    selected_action;
      
      IF selected_action IS NULL
      THEN
        PERFORM push_api_call('test_deliver', jsonb_build_object(
          'message_class'::TEXT,  'plain'::TEXT,
          'message_label'::TEXT,  'action_forbidden'::TEXT,
          'recipient'::TEXT,      input->>'player'::TEXT
        ));
        perform clear_cli_queue(input->>'player');
      ELSE
        FOR i IN
          SELECT    a.arguments
          FROM      cli_actions a
          WHERE     a.action = selected_action
          AND       a.game_phase = player_game_phase
          ORDER BY  arguments
        LOOP
          IF arguments_in_queue = i
          THEN
            SELECT  *
            FROM    cli_actions a
            WHERE   a.action    = selected_action
            AND     a.game_phase = player_game_phase
            AND     a.arguments = arguments_in_queue
            INTO    action_record;
            
            success := TRUE;
            EXIT;
          ELSIF arguments_in_queue < i
          THEN
            --SELECT  a.hints[arguments_in_queue]
            SELECT  a.hints
            FROM    cli_actions a
            WHERE   a.action    = selected_action
            AND     a.game_phase = player_game_phase
            AND     a.arguments = i
            INTO    hint;
            
            overflow := FALSE;
          END IF;
        END LOOP;

        IF success THEN
          /* Clear estimates */
          DELETE FROM estimated_values;

          /* Launch action */
          arguments := array(
            SELECT  q.token
            FROM    cli_queue q
            WHERE   q.player = input->>'player'
            AND     q.is_action = FALSE
          );
          
          IF action_record.launch_as = 'api_script'
          THEN
            buf := jsonb_build_object(
              'player'::TEXT,         input->>'player'::TEXT,
              'action'::TEXT,         array[selected_action::TEXT],
              'arguments'::TEXT,      arguments::TEXT[],
              'path'::TEXT,           action_record.call::TEXT
            );
            perform push_api_call(
              'run_api_script',
              buf
            );
          ELSIF action_record.launch_as = 'api_call'
          THEN
            buf := jsonb_build_object(
              'player'::TEXT,         input->'player'::TEXT,
              'action'::TEXT,         selected_action::TEXT,
              'arguments'::TEXT,         arguments::TEXT[]
            );
            perform push_api_call(
              action_record.call,
              buf
            );
          ELSIF action_record.launch_as = 'flag'
          THEN
            SELECT  p.avatar
            FROM    players p
            WHERE   p.username = input->>'player'
            INTO    caller;

            buf := jsonb_build_object(
              'caller'::TEXT,         caller::TEXT,
              'target'::TEXT,         arguments::TEXT[]
            );
            perform raise_flag(
              action_record.call,
              buf
            );
          ELSE
            PERFORM push_api_call('test_deliver', jsonb_build_object(
              'message_class'::TEXT,  'plain'::TEXT,
              'message_label'::TEXT,  '[BAD_ACTION_CALL]'::TEXT,
              'recipient'::TEXT,      input->>'player'::TEXT
            ));
          END IF;
          perform clear_cli_queue(input->>'player');
        ELSIF overflow THEN
            PERFORM push_api_call('test_deliver', jsonb_build_object(
              'message_class'::TEXT,  'plain'::TEXT,
              'message_label'::TEXT,  'too_many_arguments'::TEXT,
              'recipient'::TEXT,      input->>'player'::TEXT
            ));
            perform clear_cli_queue(input->>'player');
        ELSE
            PERFORM push_api_call('test_deliver', jsonb_build_object(
              'message_class'::TEXT,  'plain'::TEXT,
              'message_label'::TEXT,  hint[arguments_in_queue + 1]::TEXT,
              'recipient'::TEXT,      input->>'player'::TEXT
            ));
        END IF;
      END IF;
    END IF;
  END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION resolve_aliases(
  token   VARCHAR,
  username  VARCHAR
)
RETURNS VARCHAR AS $$
  
  DECLARE
    cur     VARCHAR;

  BEGIN
    cur := token;

    LOOP
      EXIT WHEN NOT EXISTS (
        SELECT a.alias FROM cli_aliases a
        WHERE a.alias = cur
        AND a.player = username
      );
      
      SELECT a.token FROM cli_aliases a
      WHERE a.alias = cur
      AND a.player = username
      INTO token;

      cur := token;
    END LOOP;

    RETURN cur;
  END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION normalize_language(
  token   VARCHAR,
  player  VARCHAR
)
RETURNS VARCHAR AS $$
  
  DECLARE
    cur     VARCHAR;
    lang    VARCHAR;

  BEGIN
    SELECT  language
    FROM    players
    WHERE   username = player
    INTO    lang;

    SELECT  dict.identifier
    FROM    dict_tokens dict
    WHERE   ( dict.atom = token
      OR      dict.one  = token
      OR      dict.many = token
      OR      dict.identifier = token
    ) AND (
      lang = dict.language
    )
    LIMIT   1
    INTO    cur;

    IF cur IS NULL
    THEN  RETURN token;
    ELSE  RETURN cur;
    END IF;
  END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION resolve_cli_token(
  f_token   VARCHAR,
  f_player  VARCHAR
)
RETURNS VOID AS $$
  
  DECLARE
    cur     VARCHAR;
    lang    VARCHAR;

  BEGIN
    perform log_event('info', 'resolve_cli_token', concat(f_player, ' says ', f_token));

    SELECT  language
    FROM    players
    WHERE   username = f_player
    INTO    lang;

    IF EXISTS (
      SELECT  dict.identifier
      FROM    dict_ignored dict
      WHERE   dict.language = lang
      AND     dict.identifier = f_token
    )
    THEN
      perform log_event('info', 'resolve_cli_token', concat(f_token, '... meh.'));
    ELSE
      cur := f_token;
      SELECT resolve_aliases(cur, f_player) INTO f_token;
      cur := f_token;
      SELECT normalize_language(cur, f_player) INTO f_token;
      perform log_event('info', 'resolve_cli_token', concat(f_player, ' now says ', f_token));

      IF EXISTS (
        SELECT  token
        FROM    cli_queue
        WHERE   player = f_player
        AND     is_action IS TRUE
      )
      THEN
        INSERT INTO cli_queue (
          player,     token,    is_action
        ) VALUES (
          f_player,   f_token,  FALSE
        );
      ELSIF EXISTS (
        SELECT  action
        FROM    cli_actions
        WHERE   action = f_token
      )
      THEN
        INSERT INTO cli_queue (
          player,     token,    is_action
        ) VALUES (
          f_player,   f_token,  TRUE
        );
      ELSE
        INSERT INTO cli_queue (
          player,     token,    is_action
        ) VALUES (
          f_player,   f_token,  FALSE
        );
      END IF;
    END IF;
  END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION resolve_token(
  f_token   VARCHAR,
  f_avatar  VARCHAR
)
RETURNS VARCHAR AS $$
  
  DECLARE
    f_type    VARCHAR;
    inherited VARCHAR;
    t         VARCHAR;
    k         VARCHAR;
    result    VARCHAR;
    success   BOOLEAN;

  BEGIN
    SELECT e.type FROM everything e WHERE id = f_token INTO f_type;

    IF f_type IS NULL
    THEN
      RETURN f_token;
/*    
    ELSIF f_type = 'object'
    OR    f_type = 'class'
    THEN
*/
/*
 *  The conditional we are trying is mOOt. This is one of many spots to solve.
 */
    ELSE
      FOR inherited IN
        SELECT get_all_inherited(f_token)
      LOOP
        success := TRUE;
        
        FOR t IN
          SELECT DISTINCT get_all_tags(inherited)
        LOOP
          IF t LIKE 'requires_%'
          THEN
            k := REGEXP_REPLACE(t, 'requires_', '');
            IF NOT EXISTS (
              SELECT  l.id
              FROM    links l,
                      objects o
              WHERE   l.name = 'knowledge'
              AND     l.target = f_avatar
              AND     l.id = o.id
              AND     k = any(o.classes)
            )
            THEN
              success := FALSE;
              exit;
            END IF;
          END IF;
        END LOOP;
      
        IF success AND NOT EXISTS (SELECT o.id FROM objects o WHERE o.id = inherited) THEN
          RETURN inherited;
          exit;
        END IF;
      END LOOP;
/*
    ELSE
      RETURN f_token;
*/
    END IF;
  END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION resolve_object_color(
  object_id VARCHAR
)
RETURNS VARCHAR[] AS $$
  
  DECLARE
    e         VARCHAR;
    c         VARCHAR;
    colors    VARCHAR[];

  BEGIN
    FOR e IN
      SELECT get_elements_recursive(object_id)
    LOOP
      c := get_attribute_value(e, 'color');
      IF c IS NOT NULL THEN
        colors := colors || c;
      END IF;
    END LOOP;

    RETURN colors;
  END;
$$ LANGUAGE plpgsql;


CREATE TABLE dict_tokens (
  identifier        VARCHAR,
  language          VARCHAR,
  atom              TEXT,
  one               TEXT,
  many              TEXT,
  description_short TEXT,
  description_long  TEXT[]
);

CREATE TABLE dict_labels (
  identifier    VARCHAR,
  language      VARCHAR,
  sentence      TEXT
);

CREATE TABLE dict_ignored (
  identifier        VARCHAR,
  language          VARCHAR
);

CREATE TABLE dict_help (
  identifier        VARCHAR,
  language          VARCHAR,
  game_phase        VARCHAR,
  message           TEXT[]
);

CREATE TABLE cli_aliases (
  player        VARCHAR,
  alias         VARCHAR,
  token         VARCHAR
);

/* All actions known by the mod */
CREATE TABLE cli_actions (
  action        VARCHAR,
  call          VARCHAR,
  game_phase    VARCHAR     DEFAULT 'game'::VARCHAR,
  arguments     INTEGER     DEFAULT 0,
  hints         VARCHAR[]   DEFAULT array[]::VARCHAR[],
  allows_multi  BOOLEAN     DEFAULT FALSE,
  allows_tail   BOOLEAN     DEFAULT FALSE,
  launch_as     VARCHAR     DEFAULT 'api_call'::VARCHAR
);

/* The tokens entered so far */
CREATE TABLE cli_queue (
  player    VARCHAR,
  token     VARCHAR,
  is_action BOOLEAN
);

/* JSONB buffer stored for launch */
CREATE TABLE execute_queue (
  player    VARCHAR,
  form      jsonb
);

/* Pendant flags to be resolved through cli menu */
CREATE TABLE pendant_queue (
  player      VARCHAR,
  write       VARCHAR,      -- The 'write' field for the condition
  candidates  jsonb,        -- The ids returned by the condition that overflowed, plus
                            -- the ordinal numbers that point to them in the menu
  selected    VARCHAR,      -- The id selected by the player
  call        VARCHAR,      -- Flag's call
  buffer      jsonb,        -- Flag's buffer
  run_all     BOOLEAN       DEFAULT FALSE
);

/* Pendant flags to be resolved through cli menu */
CREATE TABLE given_names_index (
  player      VARCHAR,
  target      VARCHAR,      -- The target id
  given_name  VARCHAR,      -- How is it called for <player>
  use_as      VARCHAR,      -- The 'write' field for the eventual condition
  is_fixed    BOOLEAN       DEFAULT TRUE
);


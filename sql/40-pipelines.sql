CREATE OR REPLACE FUNCTION run_trigger(
  trigger_id VARCHAR,
  buffer     jsonb
)
RETURNS jsonb AS $$
    
  DECLARE
    trigg                 RECORD;             --  Our trigger
    cond                  RECORD;             --  Current condition
    query_ids             VARCHAR[];          --  Query result
    tested_ids            VARCHAR[];          --  Ids that passed the test
    reserved_ids          VARCHAR[];          --  Ids reserved for some condition
    reserved              VARCHAR;            --  Each reserved id
    error_string          VARCHAR;            --  Indicates which item failed
    error_call            jsonb;              --  Object to be passed to the error flag
    aborted     BOOLEAN;  -- Abort conditions loop and surrender
    some_condition_failed BOOLEAN := FALSE;
    p_player  VARCHAR;  -- Player picked for pendant cli menu
    p_cnt     INTEGER;
    p_id      VARCHAR;
    p_candidates  JSONB;
    p_flag BOOLEAN DEFAULT FALSE;

  BEGIN

/*  Prepare the buffer and check everything, */

    PERFORM log_event(  'debug',
      concat(           'run_trigger ',   trigger_id  ),
                        jsonb_pretty(buffer)
    );
    
/* If caller/target is undefined, use target/caller to define it  */

    IF NOT buffer ? 'target'
    AND    buffer ? 'caller'
    THEN
      buffer := jsonb_set(
        buffer,
        '{target}',
        array_to_json( array[buffer->>'caller'] )::jsonb,
        TRUE
      );
    ELSIF   buffer ? 'target'
    AND NOT buffer ? 'caller'
    THEN
      buffer := jsonb_set(
        buffer,
        '{caller}',
        array[ json_array_elements(
          to_json(buffer->>'target'),
          1
        )]::jsonb,
        TRUE
      );
    ELSIF NOT buffer ? 'caller'
    AND   NOT buffer ? 'target'
    THEN
      /*  Must provide caller or target */
      PERFORM log_event(  'info',
        concat(           'run_trigger ', trigger_id  ),
                          'No caller or target provided'
      );
      RAISE EXCEPTION     'No caller or target provided';
    END IF;

    /*  Load the trigger into our record  */
    SELECT *
    FROM   triggers t
    WHERE  t.id = trigger_id
    INTO   trigg;

    /* Trigger's task should not be null */
    IF trigg.task IS NULL THEN
      PERFORM log_event(  'debug',
        concat(           'trigger ', trigg.id  ),
                          'Null task'
      );
      RAISE EXCEPTION     'Null task';
    END IF;

/**
 * ## CONDITIONS LOOP
 *
 * Run all conditions associated with this trigger.
 */
    aborted := false;
    FOR cond IN             --> conditions_loop
      SELECT *
      FROM   conditions c
      WHERE  c.target = trigger_id
    LOOP
      IF aborted THEN exit; END IF;

/*
 * Overwrite condition's arg with (first) target if arg is
 * null or 'TARGET'
 */
 /*TODO: add here the functionality to leave arg 'pendant' to define it by cli menu */
      IF cond.arg IS NULL
      OR cond.arg = 'TARGET'
      THEN
        SELECT buffer->'target'->>0
        INTO cond.arg;
      ELSIF cond.arg = 'CALLER'
      THEN
        SELECT buffer->>'caller'
        INTO cond.arg;
      END IF;

      /*  Log query dynamic code (for debugging)  */
      PERFORM log_event(  'debug',
        concat(           'query for condition ', cond.id ),
                          'SELECT ' || quote_ident(cond.query_name) ||
                            '(' || quote_literal(buffer->>'caller') || ');'
      );

/*TODO: cli menu: check here if there are some ids pendant */
/**
 *  Run the query, passing the caller as argument.
 *  TODO: add the option to run the query over another object
 */
      EXECUTE 'SELECT ' || quote_ident(cond.query_name) ||
        '(' || quote_literal(buffer->>'caller') || ');'
      INTO query_ids;
/*
**/
      /*  Log test dynamic code (for debugging)  */
      PERFORM log_event(  'debug',
        concat(           'test for condition ',  cond.id ),
                          'SELECT ' || quote_ident(cond.test_name) ||
                            '(' || quote_literal(query_ids) || ', ' ||
                                   quote_literal(cond.arg) || ', ' ||
                                   cond.value || ');'
      );
      
/**
 * Run the test, passing the query result, condition's arg and
 *   condition's value as arguments
 */
      EXECUTE 'SELECT ' || quote_ident(cond.test_name) ||
        '(' || quote_literal(query_ids) || ', ' ||
               quote_literal(cond.arg) || ', ' ||
               cond.value || ');'
      INTO tested_ids;
/*
**/
/* Check for reserved ids */
      IF array_upper(reserved_ids, 1) IS NOT NULL THEN
        FOREACH reserved IN ARRAY reserved_ids LOOP
          tested_ids := array_remove(tested_ids, reserved);
        END LOOP;
      END IF;

      /* Log query and test results */
      PERFORM log_event(  'debug',
        concat(           'condition ',                     cond.id ),
        concat(           'IDs that passed the query: ',    array_to_string(query_ids,  ','),
                          ' ; IDs that passed the test: ',  array_to_string(tested_ids, ',')  )
      );
/**      
 * Check required amount against tested ids
 * @CALL:cond_amount_check
**/
      IF  cond.amount = 0 and not aborted                  --> cond_amount_check
      AND array_length(tested_ids, 1) = 0
      THEN          
        IF NOT 'force_success' = any(cond.tags) THEN
/* Fail if no ids picked */
          PERFORM log_event(  'debug',
            concat(           'condition ', cond.id ),
                              'Condition failed'
          );
          
          some_condition_failed := TRUE; -- Condition failed
/* If allowed for this condition, warn the caller about the failure */
          IF 'send_call_on_failure' = any(cond.tags)
          THEN
            error_string := concat('not_enough_'::VARCHAR, cond.write::VARCHAR);
            error_call := jsonb_build_object(
              'message_class'::TEXT,  'plain'::TEXT,
              'message_label'::TEXT,  error_string::TEXT,
              'recipient'::TEXT,      buffer->'caller'::TEXT,
              'target'::TEXT,         array[buffer->>'caller'::TEXT]
            );

            PERFORM push_api_call(
              'test_deliver',
              error_call
            );
          END IF; /* Warn if allowed */
/* If allowed for this condition, abort */
          IF 'abort_on_failure' = any(cond.tags)
          THEN
            aborted := true;
          END IF; /* Abort if allowed */
        END IF; /* Fail if not ids picked */
      END IF; /* If cond.amount = 0 */

      IF cond.amount > 0 and not aborted THEN
/*TODO: cli menu: here goes the check for pendant definitions */
/* This should block running the task, but not checking for other conditions to succeed */
/* Bypass this for now */
/*
        IF cond.amount < array_length(tested_ids, 1)
        AND 'select' = any(cond.tags)
        AND NOT aborted
        THEN
          select username from players where avatar = buffer->>'caller' into p_player;
          p_cnt := 0;
          p_candidates := '{}'::jsonb;
          foreach p_id in array tested_ids loop
            p_cnt := p_cnt + 1;
            p_candidates := p_candidates || jsonb_build_object(
              p_cnt::text,  p_id::text
            );
          end loop;
          insert into pendant_queue (
            player,
            write,
            candidates,
            selected,
            call,
            buffer,
            run_all
          ) values (
            p_player,
            cond.write,
            p_candidates,
            null,
            trigg.task,
            buffer,
            false
          );
          p_flag := true;
          EXIT;
        ELSIF cond.amount <= array_length(tested_ids, 1)
*/
        IF cond.amount <= array_length(tested_ids, 1)
        THEN
          SELECT array(
            SELECT unnest(tested_ids)
            LIMIT cond.amount
          )
          INTO tested_ids; -- Grab cond.amount ids
        ELSE
          IF NOT 'force_success' = any(cond.tags) THEN
            PERFORM log_event(
              'debug',
              concat(
                'condition ',
                cond.id
              ),
              'Condition failed'
            );

            some_condition_failed := TRUE; -- Condition failed
/* If allowed for this condition, warn the caller about the failure */
            IF 'send_call_on_failure' = any(cond.tags)
            THEN
              error_string := concat('not_enough_'::VARCHAR, cond.write::VARCHAR);
              error_call := jsonb_build_object(
                'message_class'::TEXT,  'plain'::TEXT,
                'message_label'::TEXT,  error_string::TEXT,
                'recipient'::TEXT,      buffer->'caller'::TEXT,
                'target'::TEXT,         array[buffer->>'caller'::TEXT]
              );

              PERFORM push_api_call(
                'test_deliver',
                error_call
              );
            END IF; /* Warn if allowed */
/* If allowed for this condition, abort */
            IF 'abort_on_failure' = any(cond.tags)
            THEN
              aborted := true;
              EXIT;
            END IF; /* Abort if allowed */
            CONTINUE;
          END IF; /* If not force_success */
        END IF;
      END IF; /* If cond.amount > 0 */
      
      IF aborted THEN
        exit;
      ELSIF cond.write = 'TARGET'
      OR cond.write IS NULL THEN
        buffer := jsonb_set(
          buffer,
          '{target}',
          array_to_json(tested_ids)::jsonb,
          TRUE
        );
        
        IF 'reserve' = any(cond.tags)
        THEN
          reserved_ids := array_cat(reserved_ids, tested_ids);
        END IF;

      ELSIF 'write_arg' = any(cond.tags) THEN
        PERFORM log_event(  'debug',
          concat(           'write_tag for condition ', cond.id ),
          concat(           'buffer->>',                cond.write,
                            ' before: ',                jsonb_pretty(buffer->cond.write)  )
        );
        
        buffer := jsonb_set(
          buffer,
          concat('{', cond.write, '}')::TEXT[],
          array_to_json(
            array(
              SELECT jsonb_array_elements_text(buffer->cond.write)
            ) || concat(cond.arg)
          )::jsonb,
          TRUE
        );
          
        PERFORM log_event(  'debug',
          concat(           'write_tag for condition ', cond.id ),
          concat(           'buffer->>',                cond.write,
                            ' after: ',                 jsonb_pretty(buffer->cond.write)  )
        );
      ELSIF 'write_value' = any(cond.tags) THEN
        PERFORM log_event(  'debug',
          concat(           'write_value for condition ',   cond.id ),
          concat(           'buffer->>',                    cond.write,
                            ' before: ',                    jsonb_pretty(buffer->cond.write)  )
        );

        buffer := buffer || jsonb_build_object(
          cond.write::TEXT,     array[cond.value::numeric]
        );
          
        PERFORM log_event(  'debug',
          concat(           'write_value for condition ', cond.id ),
          concat(           'buffer->>',                  cond.write,
                            ' after: ',                   jsonb_pretty(buffer->cond.write)  )
        );
      ELSE
        buffer := jsonb_set(
          buffer,
          concat('{', cond.write, '}')::TEXT[],
          array_to_json(
            array(
              SELECT jsonb_array_elements_text(buffer->cond.write)
            )::VARCHAR[] || tested_ids
          )::jsonb,
          TRUE
        );
        
        IF 'reserve' = any(cond.tags)
        THEN
          reserved_ids := array_cat(reserved_ids, tested_ids);
        END IF;

      END IF;

    END LOOP; /* Conditions loop */
    
    IF 'is_modifier' = any(trigg.tags) THEN
      buffer := buffer || jsonb_build_object(
        'tested_ids'::TEXT,       tested_ids::TEXT[]
      );
      
      RETURN buffer; -- Success (modifier)      
    ELSIF some_condition_failed THEN
      perform log_event('info', '', 'Some condition failed'::TEXT);
      RETURN NULL; -- Failure      
    ELSIF some_condition_failed OR aborted THEN
      perform log_event('info', '', 'We aborted'::TEXT);
      RETURN NULL; -- Failure      
    ELSIF p_flag THEN
      perform log_event('info', 'Launching menu', jsonb_pretty(p_candidates)::TEXT);
      return buffer;
    ELSE
      IF EXISTS (
        SELECT routine_name
        FROM   information_schema.routines
        WHERE  specific_schema = (SELECT current_schema())
        AND    routine_name = trigg.task
      )
      THEN
        PERFORM log_event(  'debug',
          concat(           'run_trigger ',     trigger_id  ),
          concat(           'Executing task ',  trigg.task, ' ', jsonb_pretty(buffer) )
        );
        
        EXECUTE format(
          'SELECT FROM %s(%s::jsonb)',
          trigg.task,
          quote_literal(buffer)
        );
      
      ELSE
        PERFORM log_event(  'debug',
          concat(           'run_trigger ',       trigger_id  ),
          concat(           'Pushing API Call ',  trigg.task, ' ', jsonb_pretty(buffer) )
        );
        
        PERFORM push_api_call(
          trigg.task,
          buffer
        );
      END IF;
      
      if aborted then return null; else
        RETURN buffer; -- Success (task)
      end if;
    END IF;
  END;
$$ language plpgsql;


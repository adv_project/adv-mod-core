CREATE OR REPLACE FUNCTION resolve_flags()
RETURNS VOID AS $$
  
  DECLARE
    flag                RECORD;
    trigg               RECORD;
    target              VARCHAR;
    inherited           VARCHAR;
    all_inherited       VARCHAR[];
    trigger_result      jsonb;
    all_triggers_failed BOOLEAN := TRUE;
    trigger_found       BOOLEAN := FALSE;
  
  BEGIN
    PERFORM log_event(  'debug',
                        'flag_stack',
                        'enter LOOP'
    );

    WHILE EXISTS (
      SELECT *
      FROM   flags_stack
    )
    LOOP
      FOR flag IN
        SELECT *
        FROM   flags_stack
        ORDER BY ord
      LOOP
        PERFORM log_event(  'debug',
                            'resolve_flags',
          concat(           'Resolving call: ',  flag.call,
                            ', buffer: ',        jsonb_pretty(flag.buffer)  )
        );

        PERFORM log_event(  'debug',
                            'buffer targets',
                            'enter LOOP (get inherited classes)'
        );
        
        FOR target IN
          SELECT *
          FROM   jsonb_array_elements_text(flag.buffer->'target')
        LOOP
          all_inherited := array(
            SELECT DISTINCT i.id
            FROM get_all_inherited(target) i
          );
          
          -- Get all the triggers inherited by the target and that have flag.call as tag
          PERFORM log_event(  'debug',
                              'triggers',
                              'enter LOOP'
          );

          FOR trigg IN
            SELECT *
            FROM   triggers t
            WHERE  t.target = any(all_inherited)
            AND    flag.call = any(t.tags)
          LOOP
            
				PERFORM log_event(  'debug',
                                'resolve_flags',
              concat(           'preparing to run trigger: ',  trigg.id  )
            );

            trigger_found := TRUE;

            SELECT run_trigger
            FROM   run_trigger(
              trigg.id,
              flag.buffer
            )
            INTO trigger_result;
            
            IF trigger_result IS NOT NULL
            THEN
              PERFORM log_event(  'debug',
                                  'resolve_flags',
                concat(           'Trigger ', trigg.id, ' succeeded'  )
              );
              
              all_triggers_failed := FALSE;
              
              IF NOT flag.run_all THEN
                EXIT;
              END IF;
            END IF; -- IF trigger_result IS NOT NULL
          END LOOP; -- FOR trigg ... LOOP
        END LOOP;   -- FOR target ... LOOP
        
        IF
          NOT trigger_found
        THEN
          PERFORM log_event(  'debug',
                              'resolve_flags',
                              'No triggers found'  );
        ELSIF
          all_triggers_failed
        THEN
          PERFORM log_event(  'debug',
                              'resolve_flags',
                              'All triggers failed'
       );
/*TODO: cli menu: check here for pendant menus */
        ELSE
          PERFORM log_event(  'debug',
                              'resolve_flags',
                              'Success'
          );
        END IF;
        
        DELETE FROM flags_stack
        WHERE ord = flag.ord;
      END LOOP; -- FOR flag ... LOOP
    END LOOP;   -- WHILE EXISTS flag ... LOOP
    
    PERFORM log_event(  'debug',
                        'resolve_flags',
                        'Exiting'
   );

    RETURN;
  END;
$$ language plpgsql;

CREATE OR REPLACE FUNCTION raise_flag(
  call    VARCHAR,
  buffer  jsonb,
  run_all BOOLEAN default FALSE
)
RETURNS VOID AS $$
  
  DECLARE
    n BIGINT;
  
  BEGIN
    SELECT ord
    FROM   flags_stack
    ORDER BY ord DESC
    LIMIT 1
    INTO n;

    IF n IS NULL THEN
      n := 0;
    END IF;
    
    INSERT INTO flags_stack (
      ord,
      call,
      buffer,
      run_all
    )
    VALUES (
      n+1,
      call,
      buffer,
      run_all
    );

    RETURN;
  END;
$$ language plpgsql;

CREATE OR REPLACE FUNCTION push_api_call(
  task   VARCHAR,
  buffer jsonb
)
RETURNS VOID AS $$

  DECLARE
    n BIGINT;

  BEGIN
    SELECT ord
    FROM   api_calls_stack
    ORDER BY ord DESC
    LIMIT 1
    INTO n;

    IF n IS NULL THEN
      n := 0;
    END IF;
    
    INSERT INTO api_calls_stack (
      ord,
      call,
      buffer
    )
    VALUES (
      n+1,
      task,
      buffer
    );

    RETURN;
  END;
$$ language plpgsql;


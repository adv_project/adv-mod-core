/* Database logs */

CREATE TABLE logs (
	ts           TIMESTAMP,
	log_level    VARCHAR,
	log_class    VARCHAR,
	log_content  VARCHAR
);

/* Main index, contains all unique ids used in the game */

CREATE TABLE everything (
    id        VARCHAR PRIMARY KEY,    -- Unique, may be stone, hunger, asdf1234, ...
    type      VARCHAR,                -- May be attribute, instance, class...
    in_table  VARCHAR                 -- Table in which the object is located
);

/* Mod definitions */

-- Slots define open spaces in objects
CREATE TABLE slots (
	id         VARCHAR REFERENCES everything ON DELETE CASCADE,
	ord        INTEGER,
	name       VARCHAR,
	element    VARCHAR,
	mass       NUMERIC,
	closure    NUMERIC  DEFAULT 0.0,
	slot_type  VARCHAR  DEFAULT 'placement'::VARCHAR,
	value      NUMERIC  DEFAULT -1.0
);

-- Plugs define how objects occupy spaces
CREATE TABLE plugs (
	id         VARCHAR  REFERENCES everything ON DELETE CASCADE,
	ord        INTEGER,
	name       VARCHAR,
	element    VARCHAR,
	mass       NUMERIC,
	slot_type  VARCHAR  DEFAULT 'placement'::VARCHAR,
	value      NUMERIC  DEFAULT 1.0
);

-- Slices define the mass of objects
CREATE TABLE slices (
	id       VARCHAR REFERENCES everything ON DELETE CASCADE,
	name     VARCHAR,
   element  VARCHAR,
	mass     NUMERIC
);

CREATE TABLE triggers (
	id      VARCHAR    PRIMARY KEY REFERENCES everything ON DELETE CASCADE,
	target  VARCHAR                REFERENCES everything ON DELETE CASCADE,
	label   VARCHAR    DEFAULT 'undefined'::VARCHAR,
	task    VARCHAR    DEFAULT 'skip_task'::VARCHAR,
	tags    VARCHAR[]  DEFAULT array[]::VARCHAR[]
);

CREATE TABLE conditions (
    id         VARCHAR    PRIMARY KEY REFERENCES everything ON DELETE CASCADE,
	  target     VARCHAR                REFERENCES triggers   ON DELETE CASCADE,
    type       VARCHAR,
    write      VARCHAR,
	  label      VARCHAR    DEFAULT 'undefined'::VARCHAR,
    query_name VARCHAR    DEFAULT 'self'::VARCHAR,
    test_name  VARCHAR    DEFAULT 'always'::VARCHAR,
    arg        VARCHAR    DEFAULT 'undef'::VARCHAR,
    value      NUMERIC    DEFAULT 0.0,
    amount     NUMERIC    DEFAULT 1.0,
	  tags       VARCHAR[]  DEFAULT array[]::VARCHAR[]
);

CREATE TABLE flags_stack (
    ord     BIGINT     PRIMARY KEY  check (ord > 0),
    call    VARCHAR,
    buffer  jsonb,
    run_all BOOLEAN                 DEFAULT FALSE
);

CREATE TABLE api_calls_stack (
    ord BIGINT PRIMARY KEY CHECK (ord > 0),
    call VARCHAR,
    buffer jsonb
);

/* Game tables */

-- Links are used to connect different objects in several ways
CREATE TABLE links (
   id      VARCHAR   REFERENCES everything ON DELETE CASCADE,
	type    VARCHAR,    -- Link type (parent, neighbour, etc.)
	name    VARCHAR,    -- Link name (e.g. north)
	target  VARCHAR     -- Target object id
);

-- All objects created since world's birth go into this main table
CREATE TABLE objects (
	id       VARCHAR    PRIMARY KEY  REFERENCES everything ON DELETE CASCADE,
	classes  VARCHAR[],
	active   BOOLEAN    DEFAULT FALSE
);

-- Registered players
CREATE TABLE players (
    username        VARCHAR   PRIMARY KEY REFERENCES everything ON DELETE CASCADE,
    avatar          VARCHAR               REFERENCES everything,
    classes         VARCHAR[],
    name            VARCHAR,
    language        VARCHAR,
    connected       BOOLEAN,
    last_connected  BIGINT,
    game_phase      VARCHAR,
    race            VARCHAR,
    gender          VARCHAR,
    class           VARCHAR
);


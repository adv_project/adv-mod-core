CREATE OR REPLACE FUNCTION create_new_avatar(
  player VARCHAR,
  inherits VARCHAR[]
)
RETURNS VARCHAR AS $$
	
  DECLARE
		created_id VARCHAR;
	
  BEGIN
		created_id := make();

		UPDATE everything
    SET
      type = 'avatar',
      in_table = 'objects'
    WHERE
      id = created_id;

		INSERT INTO objects
    VALUES (
      created_id,
      inherits,
      true
    );

		UPDATE players
    SET    avatar = created_id
    WHERE   username = player;

		RETURN created_id;
	END;
$$ LANGUAGE plpgsql;


CREATE OR REPLACE FUNCTION make_phenomena(
  input jsonb
)
RETURNS VOID AS $$

  DECLARE
    string  VARCHAR;
    value   NUMERIC;
    result  NUMERIC;
    noise_label   VARCHAR;
    json    TEXT;

  BEGIN
    /* Get physical */
    IF input ? 'physical_work'
    AND jsonb_array_length(input->'physical_work') != 0
    THEN
      FOR value IN
        SELECT *
        FROM   jsonb_array_elements( input->'physical_work' )
      LOOP
        result := value * make_physical_work(input->>'caller');
        perform log_event(  'testing',
                            'make_phenomena',
          concat(           'physical work: ',  result::text, ' from value ', value::text  )
        );
      END LOOP;
    END IF;

    /* Destroy materials */
    IF input ? 'materials'
    AND jsonb_array_length(input->'materials') != 0
    THEN
      FOR string IN
        SELECT *
        FROM   jsonb_array_elements( input->'materials' )
      LOOP
        json := '{' ||
          '"object_id":' || string ||
          ',"destroy":"yes"' ||
        '}';
        
        PERFORM unlink_object(json::jsonb);
      END LOOP;
    END IF;

    /* Make noise */
    IF input ? 'noise'
    AND jsonb_array_length(input->'noise') != 0
    THEN
      FOR value IN
        SELECT *
        FROM   jsonb_array_elements( input->'noise' )
      LOOP
        IF input ? 'noise_label' THEN
          noise_label := input->'noise_label'->>0;
        ELSE
          noise_label := 'common';
        END IF;

        PERFORM make_instant_attribute(
          input->>'caller'::VARCHAR,
          'noise'::VARCHAR,
          value::NUMERIC,
          noise_label
        );
      END LOOP;
    END IF;

  END;
$$ LANGUAGE plpgsql;


/* OLD ADV MOD CODE */

/* @doc h2 Database queries*/

CREATE OR REPLACE FUNCTION is_instance(
  object_id VARCHAR
)
RETURNS BOOLEAN AS $$
  BEGIN
    RETURN EXISTS (
      SELECT TRUE
      FROM   objects o
      WHERE  o.id = object_id
    );
  END;
$$ LANGUAGE plpgsql;

-- Returns the object's type
CREATE OR REPLACE FUNCTION get_type(
  arg VARCHAR
)
RETURNS TABLE (
  type VARCHAR
)
AS $$
  BEGIN
    RETURN QUERY
      SELECT e.type
      FROM   everything e
      WHERE  e.id = arg;
  END;
$$ LANGUAGE plpgsql;

-- Returns the parent of arg, AND the matching slot_name
CREATE OR REPLACE FUNCTION get_parent_link(
  arg VARCHAR
)
RETURNS TABLE (
  id        VARCHAR,
  slot_name VARCHAR
)
AS $$
  BEGIN
    RETURN QUERY
      SELECT
        l.target id,
        l.name   slot_name
      FROM
        links l
      WHERE l.id   = arg
		AND   l.type = 'parent';
  END;
$$ LANGUAGE plpgsql;

-- Returns all classes inherited by object with id <arg> 
CREATE OR REPLACE FUNCTION get_classes(
  arg VARCHAR
)
RETURNS TABLE (
  id VARCHAR
)
AS $$
  
  DECLARE
    our_table VARCHAR;
  
  BEGIN
    SELECT e.in_table
    FROM   everything e
    WHERE  e.id = arg
    INTO   our_table;

    IF our_table <> '' THEN
      RETURN QUERY
        EXECUTE FORMAT(
          'SELECT unnest(o.classes)
           FROM   %s o
           WHERE  o.id = ''%s'' ',
          our_table,
          arg
        );
    ELSE
      RETURN QUERY
        SELECT ''::VARCHAR;
    END IF;
  END;
$$ LANGUAGE plpgsql;

-- Returns all components inherited by object with id <arg> 
CREATE OR REPLACE FUNCTION get_components(
  arg VARCHAR
)
RETURNS TABLE (
  id VARCHAR
)
AS $$
  BEGIN
    RETURN QUERY
      SELECT  l.target
      FROM    links l
      WHERE   l.id = arg
      AND     l.type = 'component'
      AND     l.name = 'material';
  END;
$$ LANGUAGE plpgsql;

-- Returns all classes inherited by object with id <arg> 
CREATE OR REPLACE FUNCTION get_unique_class(
  arg VARCHAR
)
RETURNS TABLE (
  id VARCHAR
)
AS $$
  
  DECLARE
    our_table VARCHAR;
  
  BEGIN
    SELECT e.in_table
    FROM   everything e
    WHERE  e.id = arg
    INTO   our_table;
    
    IF our_table != 'objects' THEN
      RETURN QUERY
        SELECT arg AS id;
    ELSIF our_table <> '' THEN
      RETURN QUERY
        EXECUTE FORMAT(
          'SELECT unnest(o.classes)
           FROM   objects o
           WHERE  o.id = ''%s'' ',
          our_table,
          arg
        );
    ELSE
      RETURN QUERY
        SELECT ''::VARCHAR;
    END IF;
  END;
$$ LANGUAGE plpgsql;

-- Returns all classes recursively inherited by object with id <arg>
CREATE OR REPLACE FUNCTION get_classes_recursive(
  arg VARCHAR
)
RETURNS TABLE (
  id VARCHAR
)
AS $$
  BEGIN
    RETURN QUERY
      WITH RECURSIVE my_recursive_table(current_id) AS (
          VALUES (arg)
            UNION
          SELECT get_classes(current_id)
          FROM   my_recursive_table
          WHERE  current_id IS NOT NULL
      ) SELECT
          rt.current_id AS id
        FROM
          my_recursive_table rt,
          everything         e
        WHERE e.id = rt.current_id
        AND   e.type != 'object'
        AND   e.type != 'avatar'
        AND   e.type != 'automata';
  END;
$$ LANGUAGE plpgsql;

-- Returns all components recursively inherited by object with id <arg>
CREATE OR REPLACE FUNCTION get_components_recursive(
  arg VARCHAR
)
RETURNS TABLE (
  id VARCHAR
)
AS $$
  BEGIN
    RETURN QUERY
      WITH RECURSIVE my_recursive_table(current_id) AS (
          VALUES (arg)
            UNION
          SELECT get_components(current_id)
          FROM   my_recursive_table
          WHERE  current_id IS NOT NULL
      ) SELECT
          rt.current_id AS id
        FROM
          my_recursive_table rt,
          everything         e
        WHERE e.id = rt.current_id;
  END;
$$ LANGUAGE plpgsql;

-- Returns the tags for all classes inherited by the object
CREATE OR REPLACE FUNCTION get_tags(
  arg VARCHAR
)
RETURNS TABLE (
  tag VARCHAR
)
AS $$
  
  DECLARE
    r         RECORD;
    our_table VARCHAR;
  
  BEGIN
    FOR r IN (
      SELECT c.id
      FROM get_classes_recursive(arg) c
    ) LOOP
      SELECT e.in_table
        FROM everything e
      WHERE  e.id = r.id
      INTO   our_table;

      IF our_table <> ''
      AND EXISTS (
        SELECT attname
        FROM   pg_attribute
        WHERE  attrelid = (
            SELECT oid
            FROM   pg_class
            WHERE  relname = our_table LIMIT 1
        )
        AND attname = 'tags' LIMIT 1
      )
      THEN
        RETURN QUERY
          EXECUTE FORMAT(
            'SELECT unnest(o.tags)
             FROM   %s o
             WHERE  o.id = ''%s'' ',
            our_table,
            r.id
          );
      ELSE
        RETURN QUERY
          SELECT ''::VARCHAR;
      END IF;
    END LOOP;
  END;
$$ LANGUAGE plpgsql;

--drop function IF exists get_elements;
CREATE OR REPLACE FUNCTION get_elements(
  arg VARCHAR
)
RETURNS TABLE (
  id VARCHAR
)
AS $$
  
  DECLARE
    r         RECORD;
    c         RECORD;
    our_table VARCHAR;
  
  BEGIN
    FOR r IN (
      SELECT cl.id
      FROM   get_classes_recursive(arg) cl
    )
    LOOP
      RETURN QUERY
        SELECT slot.element
        FROM   slots slot
        WHERE  slot.id = r.id
        AND    slot.element IS NOT NULL;

      RETURN QUERY
        SELECT plug.element
        FROM   plugs plug
        WHERE  plug.id = r.id
        AND    plug.element != NULL;

      RETURN QUERY
        SELECT slice.element
        FROM   slices slice
        WHERE  slice.id = r.id;
    END LOOP;

    FOR r IN (
      SELECT cm.id
      FROM   get_components_recursive(arg) cm
    )
      LOOP
      FOR c IN (
        SELECT cl.id
        FROM   get_classes_recursive(r.id) cl
      )
      LOOP
        RETURN QUERY
          SELECT slice.element
          FROM   slices slice
          WHERE  slice.id = c.id;
      END LOOP;
    END LOOP;
  END;
$$ LANGUAGE plpgsql;

--drop function IF exists get_elements_recursive;
CREATE OR REPLACE FUNCTION get_elements_recursive(
  arg VARCHAR
)
RETURNS TABLE (
  id VARCHAR
)
AS $$
  
  DECLARE
    r RECORD;
  
  BEGIN
    for r IN (
      SELECT e.id
      FROM   get_elements(arg) e
    )
    LOOP
      RETURN QUERY
        SELECT c.id
        FROM   get_classes_recursive(r.id) c;
    END LOOP;
  END;
$$ LANGUAGE plpgsql;

--drop function IF exists get_element_tags;
CREATE OR REPLACE FUNCTION get_element_tags(
  arg VARCHAR
)
RETURNS TABLE (
  tag VARCHAR
)
AS $$
  
  DECLARE
    r RECORD;
  
  BEGIN
    FOR r IN (
      SELECT e.id
      FROM   get_elements_recursive(arg) e
    )
    LOOP
      RETURN QUERY
        SELECT t.tag
        FROM   get_tags(r.id) t;
    END LOOP;
  END;
$$ LANGUAGE plpgsql;

--drop function IF exists get_all_tags;
CREATE OR REPLACE FUNCTION get_all_tags(
  arg VARCHAR
)
RETURNS TABLE (
  tag VARCHAR
)
AS $$
  BEGIN
    RETURN QUERY
      SELECT t.tag
      FROM   get_tags(arg) t;
    RETURN QUERY
      SELECT e.tag
      FROM   get_element_tags(arg) e;
  END;
$$ LANGUAGE plpgsql;

--drop function IF exists get_all_inherited;
CREATE OR REPLACE FUNCTION get_all_inherited(
  arg VARCHAR
)
RETURNS TABLE (
  id VARCHAR
)
AS $$
  BEGIN
    RETURN QUERY
      SELECT  c.id
      FROM    get_classes_recursive(arg) c;
    
	 RETURN QUERY
      SELECT e.id
      FROM   get_elements_recursive(arg) e;
    
	 RETURN QUERY
      SELECT  l.target
      FROM    links l
      WHERE   l.id = arg
      AND     l.type = 'inherits';
  END;
$$ LANGUAGE plpgsql;

-- Returns the slots provided by all classes inherited by the object
CREATE OR REPLACE FUNCTION get_slots(
  arg VARCHAR
)
RETURNS TABLE (
  id VARCHAR,
  ord integer,
  name VARCHAR,
  slot_type VARCHAR,
  value numeric,
  element VARCHAR,
  mass numeric,
  closure numeric
)
AS $$
  
  DECLARE
    r         RECORD;
    our_table VARCHAR;
  
  BEGIN
    FOR r IN (
      SELECT c.id
      FROM   get_classes_recursive(arg) c
    )
    LOOP
      RETURN QUERY
        SELECT *
        FROM   slots s
        WHERE  s.id = r.id;
    END LOOP;
  END;
$$ LANGUAGE plpgsql;

-- Returns the plugs provided by all classes inherited by the object
CREATE OR REPLACE FUNCTION get_plugs(
  arg VARCHAR
)
RETURNS TABLE (
  id VARCHAR,
  ord integer,
  name VARCHAR,
  slot_type VARCHAR,
  value numeric,
  element VARCHAR,
  mass numeric
)
AS $$
  
  DECLARE
    r         RECORD;
    our_table VARCHAR;
  
  BEGIN
    for r IN (
      SELECT c.id
      FROM   get_classes_recursive(arg) c)
    LOOP
      RETURN QUERY
        SELECT *
        FROM   plugs p
        WHERE  p.id = r.id;
    END LOOP;
  END;
$$ LANGUAGE plpgsql;

-- Returns the slices provided by all classes inherited by the object
CREATE OR REPLACE FUNCTION get_slices(
  arg VARCHAR
)
RETURNS TABLE (
  id VARCHAR,
  name VARCHAR,
  element VARCHAR,
  mass numeric
)
AS $$
  
  DECLARE
    r         RECORD;
    our_table VARCHAR;
  
  BEGIN
    for r IN (
      SELECT c.id
      FROM get_classes_recursive(arg) c
    )
    LOOP
      RETURN QUERY
        SELECT *
        FROM   slices p
        WHERE  p.id = r.id;
    END LOOP;
  END;
$$ LANGUAGE plpgsql;

/* Queries
 *
 * All queries accept one argument, id VARCHAR, AND RETURN a single-column table,
 * containing the ids of the objects that match the query.
 */

-- Returns arg's parent id
CREATE OR REPLACE FUNCTION get_parent(
  arg VARCHAR
)
RETURNS TABLE (
  id VARCHAR
)
AS $$
  BEGIN
    RETURN QUERY
      SELECT l.target AS id
      FROM   links l
      WHERE  l.id   = arg
      AND    l.type = 'parent';
  END;
$$ LANGUAGE plpgsql;

-- Returns all object that has the same parent as arg
CREATE OR REPLACE FUNCTION get_all_in_parent(
  arg VARCHAR
)
RETURNS TABLE (
  id VARCHAR
)
AS $$
  
  DECLARE
    parent_id VARCHAR;
  
  BEGIN
    SELECT p.id
    FROM   get_parent(arg) p
    INTO   parent_id;
    RETURN QUERY
      SELECT l.id
      FROM   links l
      WHERE  l.target =  parent_id
      AND    l.type   =  'parent'
      AND    l.id     != arg;
  END;
$$ LANGUAGE plpgsql;

-- Returns all objects that have arg as parent
CREATE OR REPLACE FUNCTION get_all_in_self(
  arg VARCHAR
)
RETURNS TABLE (
  id VARCHAR
)
AS $$
  BEGIN
    RETURN QUERY
      SELECT l.id
      FROM   links l
      WHERE  l.target = arg
      AND    l.type   = 'parent';
  END;
$$ LANGUAGE plpgsql;

-- Returns arg's parent, arg's parent's parent, AND so, until the object 'world'
CREATE OR REPLACE FUNCTION get_all_parents_recursive(
  arg VARCHAR
)
RETURNS TABLE (
  id VARCHAR
)
AS $$
  BEGIN
    RETURN QUERY
      WITH RECURSIVE my_recursive_table(current_id) AS (
        VALUES (arg) -- valor inicial de current_id
          UNION ALL
        SELECT get_parent(current_id)
        FROM   my_recursive_table
        WHERE  current_id IS NOT NULL
      ) SELECT current_id AS id
        FROM   my_recursive_table rt
        WHERE  current_id != arg;
  END;
$$ LANGUAGE plpgsql;

/* END OF OLD ADV MOD CODE */

/* OLD INIT HOOK CODE */

-- Returns a random alphanumeric ASCII character (0-9, A-Z or a-z)
CREATE OR REPLACE FUNCTION new_alphanum_int()
RETURNS INTEGER AS $$
  
  DECLARE
      i INTEGER;
  
  BEGIN
    /* We have 10 + 26 + 26 = 62 possible characters */
    i := round( 62*random() );

    IF    (i < 10) THEN RETURN (i + 48);  -- We picked a number
    ELSIF (i < 36) THEN RETURN (i + 55);  -- We picked an uppercase letter
    ELSIF (i < 62) THEN RETURN (i + 61);  -- We picked a lowercase letter
    ELSE                RETURN      122;  -- Border case, RETURN 'z'
    END IF;
  END;
$$ LANGUAGE plpgsql;

-- Returns a random ID of length id_length
CREATE OR REPLACE FUNCTION new_id(
  id_length INTEGER
)
RETURNS VARCHAR AS $$
  BEGIN
    RETURN array_to_string(
      array(
        SELECT chr( new_alphanum_int() )
        FROM   generate_series(1, id_length)
      ),
      ''
    );
  END;
$$ LANGUAGE plpgsql;

-- Place a new identifier inside the 'everything' table
CREATE OR REPLACE FUNCTION make()
RETURNS VARCHAR AS $$
  
  DECLARE
    created_id      VARCHAR;
    id_length       INTEGER := 4; -- Initial length for new IDs
    number_of_tries INTEGER;
  
  BEGIN
    LOOP
      number_of_tries := 0;

      LOOP
        created_id := new_id(id_length);
        EXIT WHEN NOT EXISTS (
          SELECT o.id
          FROM   objects o
          WHERE created_id = o.id
        );
        number_of_tries := number_of_tries + 1;
        EXIT WHEN number_of_tries = 10; -- Number of tries before abandoning this ID length
      END LOOP;

      EXIT WHEN NOT EXISTS (
        SELECT o.id
        FROM   objects o
        WHERE  created_id = o.id
      );

      id_length := id_length + 1;
    END LOOP;

    INSERT INTO everything
    VALUES (created_id);

    RETURN created_id;
  END;
$$ LANGUAGE plpgsql;

/* END OF OLD INIT HOOK CODE */

-- Returns all slots associated with an object
CREATE OR REPLACE FUNCTION object_slots(
  object_id VARCHAR
)
RETURNS SETOF slots AS $$
  
  DECLARE
    inherited VARCHAR[];
  
  BEGIN
    inherited := array(
      SELECT i.id
      FROM   get_all_inherited(object_id) i
    );
    RETURN QUERY
      SELECT   *
      FROM     slots s
      WHERE    s.id = any(inherited)
      ORDER BY ord;
  END;
$$ LANGUAGE plpgsql;

-- Returns the unique object's slot that matches given name
CREATE OR REPLACE FUNCTION object_unique_slot(
  object_id VARCHAR,
  slot_name VARCHAR
)
RETURNS SETOF slots AS $$
  
  DECLARE
    inherited VARCHAR[];
  
  BEGIN
    inherited := array(
      SELECT i.id
      FROM   get_all_inherited(object_id) i
    );
    RETURN QUERY
      SELECT  *
      FROM    slots s
      WHERE   s.id = any(inherited)
      AND     s.name = slot_name;
  END;
$$ LANGUAGE plpgsql;

-- Returns the parent's slot where object is placed
CREATE OR REPLACE FUNCTION object_parent_slot(
  object_id VARCHAR
)
RETURNS SETOF slots AS $$
  
  DECLARE
    parent_link   RECORD;
  
  BEGIN
    SELECT  l.name,
            l.target
    FROM    links l
    WHERE   l.id = object_id
    AND     l.type = 'parent'
    INTO    parent_link;

    RETURN QUERY
      SELECT * FROM object_unique_slot(parent_link.target, parent_link.name);
  END;
$$ LANGUAGE plpgsql;

-- Returns all plugs associated with an object
CREATE OR REPLACE FUNCTION object_plugs(
  object_id VARCHAR
)
RETURNS SETOF plugs AS $$
  
  DECLARE
    inherited VARCHAR[];
  
  BEGIN
    inherited := array(
      SELECT i.id
      FROM   get_all_inherited(object_id) i
    );
    RETURN QUERY
      SELECT   *
      FROM     plugs p
      WHERE    p.id = any(inherited)
      ORDER BY ord;
  END;
$$ LANGUAGE plpgsql;

-- Returns all slices associated with an object
CREATE OR REPLACE FUNCTION object_slices(
  object_id VARCHAR
)
RETURNS SETOF slices AS $$
  
  DECLARE
    inherited VARCHAR[];
    r RECORD;
    c RECORD;
  
  BEGIN
    inherited := array(
      SELECT i.id
      FROM   get_all_inherited(object_id) i
    );
    RETURN QUERY
      SELECT   *
      FROM     slices p
      WHERE    p.id = any(inherited);

    FOR r IN (
      SELECT cm.id
      FROM   get_components_recursive(object_id) cm
    )
      LOOP
      FOR c IN (
        SELECT cl.id
        FROM   get_classes_recursive(r.id) cl
      )
      LOOP
        RETURN QUERY
          SELECT *
          FROM   slices slice
          WHERE  slice.id = c.id;
      END LOOP;
    END LOOP;
  END;
$$ LANGUAGE plpgsql;

/* Returns a table with the values needed to link a pair of objects as parent AND child.
  Mandatory fields IN the input are the parent AND child ids.
  Optional fields are: (slot_type or slot_name) AND (plug_type or plug_name).
 */
CREATE OR REPLACE FUNCTION prepare_parent_link(
  input jsonb
)
RETURNS TABLE (
  success   BOOLEAN,
  slot_name VARCHAR
)
AS $$
  
  DECLARE
    available_space  NUMERIC;
    required_space   NUMERIC;
    winner_slot_name VARCHAR;
    sn               VARCHAR;
    cid              VARCHAR;
    sz               NUMERIC;
    csz              NUMERIC;
    st               VARCHAR;
    slot_value       NUMERIC;
  
  BEGIN    
    IF NOT input ? 'child'
    OR NOT input ? 'parent'
    THEN
      /* Input must have keys 'child' AND 'parent' */
      PERFORM log_event(	'info',
        concat(				'prepare_parent_link ',	jsonb_pretty(input)	),
							      'No child or parent provided'
      );
      RAISE EXCEPTION 		'No child or parent provided';

      RETURN QUERY
        SELECT
          FALSE       AS success,
          ''::VARCHAR AS slot_name;
    ELSE
      PERFORM log_event(	'debug',
        concat(				'prepare_parent_link ',	jsonb_pretty(input)	),
        							'BEGIN:'
      );

      /* Gather all candidates to fill the link requirements */
      
      -- Fill the slots records using whatever is available
      IF
        input ? 'slot_name'
      THEN
        CREATE TEMP TABLE parent_slots ON COMMIT DROP AS
          SELECT *
          FROM   object_slots(input->>'parent')
          WHERE  name = input->>'slot_name';
      ELSIF
        input ? 'slot_type'
      THEN
        CREATE TEMP TABLE parent_slots ON COMMIT DROP AS
          SELECT *
          FROM   object_slots(input->>'parent')
          WHERE  slot_type = input->>'slot_type';
      ELSE
        CREATE TEMP TABLE parent_slots ON COMMIT DROP AS
          SELECT *
          FROM   object_slots(input->>'parent');
      END IF;
      
      -- Fill the plugs records
      IF
        input ? 'plug_name'
      THEN
        CREATE TEMP TABLE child_plugs ON COMMIT DROP AS
          SELECT *
          FROM   object_plugs(input->>'child')
          WHERE  name = input->>'plug_name';
      ELSIF
        input ? 'plug_type'
      THEN
        CREATE TEMP TABLE child_plugs ON COMMIT DROP AS
          SELECT *
          FROM   object_plugs(input->>'child')
          WHERE  slot_type = input->>'plug_type';
      ELSE
        CREATE TEMP TABLE child_plugs ON COMMIT DROP AS
          SELECT *
          FROM   object_plugs(input->>'child');
      END IF;
      
      /* Test for slot_type matches AND free space */

      -- Get the parent slots that match some child plug
      CREATE TEMP TABLE winner_slots ON COMMIT DROP AS
        SELECT   *
        FROM     parent_slots p
        WHERE    p.slot_type = any(
          array(
            SELECT DISTINCT slot_type
            FROM child_plugs
          )
        )
        ORDER BY ord;

      -- Check for empty space IN each slot
      FOREACH sn IN ARRAY array(
        SELECT name
        FROM   winner_slots
      )
      LOOP
        SELECT value
        FROM   winner_slots
        WHERE  name = sn
        INTO   slot_value;
        
        IF (slot_value = -1) THEN
          PERFORM log_event(
            'debug',
            'check for slot space',
            concat(
              'slot ',    sn,
              ', value ', slot_value::text,
              ': success'
            )
          );
          winner_slot_name := sn;
          EXIT;
        END IF;
        
        SELECT slot_type
        FROM   winner_slots
        WHERE  name = sn
        INTO   st;
        
        -- Sum the size of all childs already located IN this slot
        sz := 0.0;

        FOREACH cid IN ARRAY array(
          SELECT id
          FROM   links
          WHERE  target = input->>'parent'
          AND    name = sn
        )
        LOOP
          SELECT value
          FROM   object_plugs(cid)
          WHERE  slot_type = st
          INTO   csz;

          SELECT sz + csz
          INTO   sz;
        END LOOP;

        -- Substract FROM the slot capacity
        SELECT slot_value - sz
        INTO   available_space;

        -- AND compare with the plug's size
        SELECT value
        FROM   child_plugs
        WHERE  slot_type = st
        LIMIT  1
        INTO   required_space;
          
        PERFORM log_event(
          'debug',
          'check for slot space',
          concat(
            'slot ',        sn,
            ', value ',     slot_value::text,
            ', available ', available_space::text,
            ', required ',  required_space::text)
        );

        IF (available_space >= required_space) THEN
          winner_slot_name := sn;
          EXIT;
        END IF;
      END LOOP;

      /* Hopefully, IF sn exists, we have succeeded */

      IF winner_slot_name IS NOT NULL THEN
        -- All ok, RETURN the checked slot name
        PERFORM log_event(
          'info',
          'prepare_parent_link',
          concat(
            'Success: ',
            winner_slot_name::text
          )
        );

        RETURN QUERY
          SELECT
            TRUE             AS success,
            winner_slot_name AS slot_name;
      ELSE
        -- No plug could fit in
        PERFORM log_event(
          'info',
          'prepare_parent_link',
          'Failed'
        );

        RETURN QUERY
          SELECT
            FALSE       AS success,
            ''::VARCHAR AS slot_name;
      END IF;

      DROP TABLE parent_slots;
      DROP TABLE child_plugs;
      DROP TABLE winner_slots;
    END IF;
  END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION get_parent_tile(
  object_id VARCHAR
)
RETURNS VARCHAR AS $$
  
  DECLARE
    cur      VARCHAR;
    parent   VARCHAR;
    previous VARCHAR;
  
  BEGIN
    SELECT object_id  INTO cur;
    SELECT cur        INTO previous;
    
    PERFORM log_event(
      'debug',
      'get_parent_tile ',
      cur
    );

    LOOP
      SELECT l.target
      FROM   links l
      WHERE  l.type = 'parent'
      AND    l.id   = cur
      INTO parent;

      IF parent IS NULL THEN
        PERFORM log_event(
          'debug',
          'parent tile found',
          previous
        );
        EXIT;
      END IF;

      SELECT cur    INTO previous;
      SELECT parent INTO cur;
      
      PERFORM log_event(
        'debug',
        'descending into',
        cur
      );
    END LOOP;
    
    RETURN previous;
  END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION activate_object(
  object_id VARCHAR
)
RETURNS INTEGER AS $$
  
  DECLARE
    inherited VARCHAR;
  
  BEGIN
    PERFORM log_event('debug',
      'activate_object ',
      object_id
    );

    -- Mark as active
    UPDATE objects
    SET    active = TRUE
    WHERE  id     = object_id;

    -- Run on_activate hook
    FOR inherited IN
      SELECT get_all_inherited(object_id)
    LOOP
      IF EXISTS (
        SELECT routine_name
        FROM   information_schema.routines
        WHERE  specific_schema = ( SELECT current_schema() )
        AND    routine_name = 'on_activate_' || quote_ident(inherited))
      THEN
        EXECUTE 'SELECT * FROM on_activate_' ||
          quote_ident(inherited) ||
          '(' ||
          quote_literal(object_id) ||
          ');';
      END IF;
    END LOOP;

    RETURN 0;
  END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION activate_surroundings(
  object_id VARCHAR
)
RETURNS INTEGER AS $$
  
  DECLARE
    parent_tile VARCHAR;
    tile        VARCHAR;
    object      VARCHAR;
  
  BEGIN
    SELECT get_parent_tile(object_id)
    INTO parent_tile;

    PERFORM log_event('debug',
      'activate_surroundings ',
      concat(
        object_id,
        ' on parent tile ',
        parent_tile
      )
    );
    FOR tile IN
      SELECT DISTINCT o.id
      FROM
        objects o,
        links   l
      WHERE (
        o.id     = parent_tile AND
        o.active = FALSE
      ) OR (
        o.id     = l.target    AND
        l.id     = parent_tile AND
        l.type   = 'neighbour' AND
        o.active = FALSE
      )
    LOOP
      PERFORM log_event(
        'debug',
        'neighbour ',
        tile
      );

      UPDATE objects
      SET    active = TRUE
      WHERE  id = tile;

      FOR object IN
        SELECT all_in_self(tile)
      LOOP
        UPDATE objects
        SET    active = true
        WHERE  id = object;
        --PERFORM activate_object(object);
      END LOOP;
    END LOOP;

    RETURN 0;
  END;
$$ LANGUAGE plpgsql;

/* Puts an object IN a new parent, IF possible.
  Mandatory fields IN the input are child AND parent.
  Optional fields are:
  (slot_type or slot_name) AND (plug_type or plug_name): WHERE to place the object IN the parent
 */
CREATE OR REPLACE FUNCTION put_object_in_place(
  input    jsonb,
  activate BOOLEAN DEFAULT FALSE
)
RETURNS TABLE (
  success   BOOLEAN,
  slot_name VARCHAR
)
AS $$
  
  DECLARE
    placement_result RECORD;
  
  BEGIN
    IF
      NOT input ? 'child'
    OR
      NOT input ? 'parent'
    THEN
      /* Input must have keys 'child' AND 'parent' */
      PERFORM log_event(
        'info',
        concat(
          'put_object_in_place ',
          jsonb_pretty(input)
        ),
        'No child or parent provided'
      );

      RAISE EXCEPTION 'No child or parent provided';

      RETURN QUERY
        SELECT
          FALSE       AS success,
          ''::VARCHAR AS slot_name;
    ELSE
      /* Attempt to place the object IN the parent */
      SELECT *
      FROM   prepare_parent_link(input)
      INTO   placement_result;

      IF placement_result.success THEN
        -- Remove the old link
        DELETE FROM links
        WHERE  type = 'parent'
        AND    id = input->>'child';
        
        -- Link child AND parent
        INSERT INTO links
        VALUES (
          input->>'child',
          'parent',
          placement_result.slot_name,
          input->>'parent'
        );

        -- Activate surroundings
        IF activate THEN
          PERFORM activate_object(input->>'child');
          PERFORM activate_surroundings(input->>'child');
        END IF;

        /* Unlink if the object was being used */
        DELETE FROM links WHERE target = input->>'child' AND type = 'use';
        /* Unlink if the object was using something */
        DELETE FROM links WHERE id = input->>'child' AND type = 'use';

        RETURN QUERY
          SELECT
            placement_result.success,
            placement_result.slot_name;
      ELSE
        RETURN QUERY
          SELECT
            FALSE       AS success,
            ''::VARCHAR AS slot_name;
      END IF;
      /* Return the output of prepare_parent_link as is */
    END IF;
  END;
$$ LANGUAGE plpgsql;

/* Registers an object IN everything AND fill class, links, type, ...
  Mandatory fields IN the input are the object class AND the parent id.
  Optional fields are:
  (slot_type or slot_name) AND (plug_type or plug_name): WHERE to place the object IN the parent
  noactive: Do not activate the new object
  init: IF this field exists, attempt to run the init callback for the object (named init_<class>()
  IN the database).
  spawn: IF this field exists, attempt to run the spawn callback for the object (named spawn_<class>()
  IN the database).
 */
CREATE OR REPLACE FUNCTION register_new_object(
  input    jsonb,
  activate BOOLEAN DEFAULT FALSE
)
RETURNS VARCHAR AS $$
  
  DECLARE
    new_id           VARCHAR;
    placement_result RECORD;
    routine          VARCHAR;
  
  BEGIN
    IF
      NOT input ? 'class'
    OR
      NOT input ? 'parent'
    THEN
      /* Input must have keys 'class' AND 'parent' */
      PERFORM log_event(
        'info',
        concat(
          'register_new_object ',
          jsonb_pretty(input)
        ),
        'No class or parent provided'
      );

      RAISE EXCEPTION 'No class or parent provided';

      RETURN ''::VARCHAR;
    ELSE
      /* Register the new guest IN the main index */

      SELECT *
      FROM   make()
      INTO   new_id;
      
      IF input ? 'is_avatar' THEN
        UPDATE everything
        SET
          type     = 'avatar',
          in_table = 'objects'
        WHERE
          id = new_id;
        
        INSERT INTO objects
        VALUES (
          new_id,
          array[
            input->>'class',
            input->>'race',
            input->>'gender',
            'console'::VARCHAR
          ],
          FALSE
        );
      ELSE
        UPDATE everything
        SET
          type     = 'object',
          in_table = 'objects'
        WHERE
          id = new_id;

        INSERT INTO objects
        VALUES (
          new_id,
          array[input->>'class'],
          false
        );
      END IF;

      /* Put the object IN place */

      -- Prepare the input object
      input := input ||
        jsonb_build_object(
          'child'::text,  new_id
        );

      --  Attempt to place the object IN the parent
      SELECT *
      FROM   put_object_in_place(
        input::jsonb,
        activate
      )
      INTO   placement_result;

      -- TODO: perhaps this should be written INTO another function
      IF placement_result.success THEN
        -- Activate the object
        IF NOT input ? 'noactive' THEN
          UPDATE objects
          SET    active = true
          WHERE  id = new_id;
        END IF;

        -- Run init AND spawn routines, IF asked so
        FOREACH routine IN ARRAY
          array['on_init', 'on_spawn']
        LOOP
          IF input ? routine
          AND EXISTS (
            SELECT routine_name
            FROM   information_schema.routines
            WHERE  specific_schema = ( SELECT current_schema() )
            AND    routine_name = quote_ident(routine) ||
              '_' ||
              quote_ident(input->>'class')
          )
          THEN
            EXECUTE 'SELECT * FROM ' ||
              quote_ident(routine) ||
              '_' ||
              quote_ident(input->>'class') ||
              '(' ||
              quote_literal(new_id) ||
              ');';
          END IF;
        END LOOP;
      END IF;
    END IF;

    /* All done */
    RETURN new_id;
   END;
$$ LANGUAGE plpgsql;

/* Deletes an object.
  Field object_id IN the input is mandatory.
  Optional fields are:
  destroy: IF this field exists, attempt to run the destroy callback for the object (named on_destroy_<class>()
  IN the database).
 */
CREATE OR REPLACE FUNCTION unlink_object(
  input jsonb
)
RETURNS VARCHAR AS $$
  
  DECLARE
    classes VARCHAR;
    routine VARCHAR;
  
  BEGIN
    IF NOT input ? 'object_id' THEN
      /* Input must have keys 'object_id' AND 'parent' */
      PERFORM log_event(
        'info',
        concat(
          'unlink_object ',
          jsonb_pretty(input)
        ),
        'No object_id provided'
      );
      
      RAISE EXCEPTION 'No object_id provided';
      
      RETURN ''::VARCHAR;
    ELSE
      SELECT get_classes(input->>'object_id')
      INTO classes;

      -- Run init AND spawn routines, IF asked so
      FOREACH routine IN ARRAY
        array['on_destroy']
      LOOP
        IF input ? routine
        AND EXISTS (
          SELECT routine_name
          FROM   information_schema.routines
          WHERE  specific_schema = ( SELECT current_schema() )
          AND    routine_name = quote_ident(routine) ||
            '_' ||
            quote_ident(classes)
        )
        THEN
          EXECUTE 'SELECT * FROM ' ||
            quote_ident(routine) || '_' || quote_ident(classes) ||
            '(' || quote_literal(input->>'object_id') || ');';
        END IF;
      END LOOP;
      -- Destroy the object
      -- Notice that we are no longer able to invoke it, so be careful about flags, etc
      DELETE FROM everything
      WHERE id = input->>'object_id';
    END IF;

    /* All done */
    RETURN classes::VARCHAR;
   END;
$$ LANGUAGE plpgsql;


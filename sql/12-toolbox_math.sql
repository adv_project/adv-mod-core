CREATE OR REPLACE FUNCTION attr_to_coef(
  value         NUMERIC,
  coefficient   NUMERIC
)
RETURNS NUMERIC AS $$
  BEGIN
    IF value = 0.0 THEN value = 1.0; END IF;
    IF coefficient = 0.0 THEN coefficient = 1.0; END IF;

    RETURN
      ln(value/coefficient);
  END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION attr_to_increment(
  value         NUMERIC,
  coefficient   NUMERIC
)
RETURNS NUMERIC AS $$
  BEGIN
    RETURN
      1 + attr_to_coef(value, coefficient);
  END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION attr_to_decrement(
  value         NUMERIC,
  coefficient   NUMERIC
)
RETURNS NUMERIC AS $$
  BEGIN
    RETURN
      1 - attr_to_coef(value, coefficient);
  END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION make_physical_work(
  object_id    VARCHAR
)
RETURNS NUMERIC AS $$
  DECLARE
    object_strength        NUMERIC;
    object_constitution    NUMERIC;
    result              NUMERIC;
  BEGIN
    object_strength      := estimate_attribute(object_id,   'strength');
    object_constitution  := estimate_attribute(object_id,   'constitution');
    result := attr_to_decrement(object_constitution, 64) * attr_to_decrement(object_strength, 96);
    --IF result < 0.0 THEN result := 0; END IF;
    RETURN result;
  END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION make_physical_damage(
  hot_object    VARCHAR,
  cold_object   VARCHAR
)
RETURNS NUMERIC AS $$
  DECLARE
    hot_strength        NUMERIC;
    cold_strength       NUMERIC;
    hot_constitution    NUMERIC;
    cold_constitution   NUMERIC;
    applied_force       NUMERIC;
    resistance          NUMERIC;
    result              NUMERIC;
  BEGIN
    hot_strength      := estimate_attribute(hot_object,   'strength');
    cold_strength     := estimate_attribute(cold_object,  'strength');
    hot_constitution  := estimate_attribute(hot_object,   'constitution');
    cold_constitution := estimate_attribute(cold_object,  'constitution');
    applied_force := attr_to_coef(hot_strength, 6) * attr_to_increment(hot_constitution, 48);
    resistance    := attr_to_coef(cold_constitution, 6) * attr_to_increment(cold_strength, 48);
    result := applied_force / resistance;
    --IF result < 0.0 THEN result := 0; END IF;
    RETURN result;
  END;
$$ LANGUAGE plpgsql;


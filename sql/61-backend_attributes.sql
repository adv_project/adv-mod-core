CREATE OR REPLACE FUNCTION resolve_boundaries(
  attribute VARCHAR,
  old_value NUMERIC,
  new_value NUMERIC
)
RETURNS TABLE (
  direction  VARCHAR,
  boundaries VARCHAR[]
)
AS $$

  DECLARE
    boundary RECORD;
    reached  BOOLEAN := false;
    ret      VARCHAR[];
    
  BEGIN
    IF old_value < new_value THEN
      FOR boundary IN
        SELECT   *
        FROM     boundaries
        WHERE    id = attribute
        ORDER BY value
      LOOP
        IF old_value <= boundary.value THEN
          reached := true;
        END IF;
        
        IF  reached
        AND new_value > boundary.value
        THEN
          ret := ret || array(
            SELECT boundary.label
          );
        END IF;
      END LOOP;

      IF ret IS NOT NULL THEN
        RETURN QUERY
          SELECT
            'up'::VARCHAR   AS direction,
            ret::VARCHAR[]  AS boundaries;
      END IF;
    ELSIF old_value > new_value THEN
      FOR boundary IN
        SELECT   *
        FROM     boundaries
        WHERE    id = attribute
        ORDER BY value DESC
      LOOP
        IF old_value > boundary.value THEN
          reached := true;
        END IF;
        
        IF reached AND new_value <= boundary.value THEN
          ret := ret || array(
            SELECT boundary.label
          );
        END IF;
      END LOOP;
        
      IF ret IS NOT NULL THEN
        RETURN QUERY
          SELECT
            'down'::VARCHAR AS direction,
            ret::VARCHAR[]  AS boundaries;
      END IF;
    END IF;
  END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION get_attribute_value(
  id        VARCHAR,
  attribute VARCHAR
)
RETURNS NUMERIC AS $$
  
  DECLARE
    table_name VARCHAR;
    value      NUMERIC;
  
  BEGIN
     SELECT x.in_table
    FROM   everything x
    WHERE  x.id = attribute
    INTO   table_name;

    IF table_name IS NULL THEN
      RETURN NULL;
    ELSE
      EXECUTE format(
        'SELECT %I
         FROM %I
         WHERE id = %L;',
        attribute,
        table_name,
        id
      )
      INTO value;
       
      RETURN value::NUMERIC;
    END IF;
  END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION insert_attribute_value(
  id        VARCHAR,
  attribute VARCHAR,
  value     NUMERIC
)
RETURNS NUMERIC AS $$
  
  DECLARE
    table_name VARCHAR;
  
  BEGIN
     SELECT x.in_table
    FROM   everything x
    WHERE  x.id = attribute
    INTO   table_name;
    
    IF table_name is NULL THEN
      RETURN NULL;
    ELSE
      EXECUTE format(
        'update %I
         set    %I = $1
         WHERE  id = %L;',
        table_name,
        attribute,
        id
      )
      USING value;
       
      RETURN value;
    END IF;
  END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION initialize_attribute(
  object_id VARCHAR,
  attribute VARCHAR
)
RETURNS NUMERIC AS $$
  
  DECLARE
    table_name        VARCHAR;
    inherited         VARCHAR;
    value             NUMERIC;
    already_in_table  VARCHAR;
  
  BEGIN
     SELECT in_table
    FROM   everything
    WHERE  id = attribute
    INTO   table_name;

    IF table_name is NULL THEN
      RETURN NULL;
    ELSE
      FOREACH inherited IN ARRAY array(
        SELECT i.id
        FROM   get_all_inherited(object_id) i
      )
      LOOP
        EXECUTE format(
          'SELECT %I
           FROM   %I
           WHERE  id = %L;',
          attribute,
          table_name,
          inherited
        )
        INTO value;

        IF value IS NOT NULL THEN
           EXECUTE format(
            'SELECT id
             FROM   %I
             WHERE  id = %L;',
            table_name,
            object_id
          )
          INTO already_in_table;

          IF already_in_table IS NOT NULL THEN
             EXECUTE format(
              'UPDATE %I
               SET    %I = $1
               WHERE  id = %L;',
              table_name,
              attribute,
              object_id
            )
            USING value;
          ELSE
             EXECUTE format(
              'INSERT INTO %I (
                 id,
                 %I
               )
               VALUES (
                 %L,
                 $1
               );',
              table_name,
              attribute,
              object_id
            )
            USING value;
          END IF;
          EXIT;
        END IF;
      END LOOP;
    END IF;

     RETURN value;
  END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION is_routine_name(
  arg   VARCHAR
)
RETURNS BOOLEAN AS $$
  BEGIN
    IF EXISTS (
      SELECT routine_name
      FROM   information_schema.routines
      WHERE  specific_schema = ( SELECT current_schema() )
      AND    routine_name = arg
    )
    THEN  RETURN TRUE;
    ELSE  RETURN FALSE;
    END IF;
  END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION propagate_attribute(
  object_id       VARCHAR,
  attribute_name  VARCHAR,
  skip_init       BOOLEAN   DEFAULT FALSE
)
RETURNS NUMERIC AS $$
  
  DECLARE
    value         NUMERIC;

  BEGIN
    RETURN value;
  END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION estimate_attribute(
  object_id VARCHAR,
  attribute_name VARCHAR,
  skip_init boolean   DEFAULT FALSE
)
RETURNS NUMERIC AS $$
  
  DECLARE
    value         NUMERIC;
    buffer        jsonb;
    parent    VARCHAR;
    tmpvalue  NUMERIC;
    modifiers     jsonb[];
    trigg         RECORD;
    this_modifier jsonb;
    m             NUMERIC;
    t             VARCHAR;
    len           integer;
    success       BOOLEAN;
    factor        NUMERIC;
    delta         NUMERIC;
    floor         NUMERIC;
    ceiling       NUMERIC;
    set_value     NUMERIC;
    fix_value     NUMERIC;
  
  BEGIN
    SELECT 1.0::NUMERIC INTO factor;
    SELECT 0.0::NUMERIC INTO delta;
    SELECT FALSE        INTO success;

    SELECT  e.value
    FROM    estimated_values e
    WHERE   e.id = object_id
    AND     e.attribute = attribute_name
    INTO    value;

    IF value IS NOT NULL
    THEN
/* If it was estimated earlier and we saved the value, use it */
      RETURN value;
    ELSE
/* Try to read the attribute value FROM the table, IF exists */
      value := get_attribute_value(
        object_id,
        attribute_name
      );

/* Try to initialize it FROM inherited classes IF not found */
      IF value IS NULL
      AND NOT skip_init
      THEN
        value := initialize_attribute(
          object_id,
          attribute_name
        );
      END IF;

/* If it's not defined IN the classes, maybe it is a "volatile" attribute
   (not defined in the mods but intended to be calculated on the fly).
   See IF there's a function that calculates it
*/
      IF value IS NULL
      AND is_routine_name('calculate_' || attribute_name)
      THEN
        EXECUTE format(
          'SELECT %I(%L);',
          'calculate_' || attribute_name,
          object_id
        )
        INTO value;
      END IF;

/* See if any of the parents defines the attribute */
      IF value IS NULL
      THEN
        FOR parent IN
          SELECT get_all_parents_recursive(object_id)
        LOOP
          tmpvalue := estimate_attribute(
            parent,
            attribute_name
          );

          IF tmpvalue IS NOT NULL
          THEN
            value := tmpvalue;
            EXIT;
          END IF;
        END LOOP;
      END IF;

      IF value IS NOT NULL THEN
        -- Process modifiers
        buffer := jsonb_build_object(
          'caller'::text,   object_id::text
        );

        -- Do we really need this...?
        FOR trigg IN
          SELECT *
          FROM   triggers t
          WHERE  t.target = any(
            array( SELECT get_all_inherited(buffer->>'caller') )
          )
          AND    'modifies_'::VARCHAR || attribute_name::VARCHAR = any(t.tags)
        LOOP
          SELECT run_trigger
          FROM   run_trigger(
            trigg.id,
            buffer
          )
          INTO   this_modifier;
          
          SELECT json_array_length( to_json(this_modifier->'tested_ids') )
          INTO len;
          
          IF len > 0 THEN
            success := TRUE;

            -- AppEND to our schedule
            IF this_modifier ? 'factor' THEN
              FOREACH m IN ARRAY
                array[ json_array_elements( to_json(this_modifier->'factor') ) ]
              LOOP
                factor := factor * m;
              END LOOP;

            ELSIF this_modifier ? 'delta' THEN
              FOREACH m IN ARRAY
                array[ json_array_elements( to_json(this_modifier->'delta') ) ]
              LOOP
                delta := delta + m;
              END LOOP;

            ELSIF this_modifier ? 'floor' THEN
              FOREACH m IN ARRAY
                array[ json_array_elements( to_json(this_modifier->'floor') ) ]
              LOOP
                IF floor IS NULL OR floor < m THEN floor := m; END IF;
              END LOOP;

            ELSIF this_modifier ? 'ceiling' THEN
              FOREACH m IN ARRAY
                array[ json_array_elements( to_json(this_modifier->'ceiling') ) ]
              LOOP
                IF ceiling IS NULL OR ceiling > m THEN ceiling := m; END IF;
              END LOOP;

            ELSIF this_modifier ? 'set' THEN
              FOREACH m IN ARRAY
                array[ json_array_elements( to_json(this_modifier->'set') ) ]
              LOOP
                set_value := m;
              END LOOP;

            ELSIF this_modifier ? 'fix' THEN
              FOREACH m IN ARRAY
                array[ json_array_elements( to_json(this_modifier->'fix') ) ]
              LOOP
                fix_value := m;
              END LOOP;

            END IF;
          END IF;
        END LOOP;

        -- Apply all modifiers
        IF fix_value IS NOT NULL THEN
          value := fix_value;
        ELSIF success THEN
          IF set_value IS NOT NULL THEN
            value := set_value;
          END IF;
          
          value := (value + delta)*factor;
        END IF;
        IF floor IS NOT NULL AND value < floor THEN
          value := floor;
        END IF;
        IF ceiling IS NOT NULL AND value > ceiling THEN
          value := ceiling;
        END IF;
      END IF;

      -- Save the value for later use
      IF value IS NOT NULL
      THEN
        INSERT INTO estimated_values (
          id,
          attribute,
          value
        )
        VALUES (
          object_id,
          attribute_name,
          value
        );
      END IF;

      RETURN value;
    END IF;
  END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION modify_attribute(
  object_id VARCHAR,
  attribute VARCHAR,
  modifier  VARCHAR,
  value     NUMERIC,
  string    VARCHAR   DEFAULT 'common'
)
RETURNS NUMERIC AS $$
  
  DECLARE
     attr_record         RECORD; 
    obj_is_instance     BOOLEAN;
    avatar_is_connected BOOLEAN;
    old_value           NUMERIC;
    new_value           NUMERIC;
     boundaries_record   RECORD;
    input               jsonb;
  
  BEGIN
    SELECT  in_table,
            type
    FROM    everything
    WHERE   id = attribute
    INTO    attr_record;

    SELECT  connected
    FROM    players
    WHERE   avatar = object_id
    INTO    avatar_is_connected;

    SELECT  is_instance(object_id)
    INTO    obj_is_instance;

    IF attr_record.type != 'attribute'
    OR attr_record.in_table IS NULL
    THEN
      PERFORM log_event(  'info',
        concat(           'modify_attribute ',
                            object_id, ' ',
                            attribute, ' ',
                            modifier, ' ',
                            value ),
                          'argument is not an attribute or has no table');
      RETURN NULL;
    ELSIF NOT obj_is_instance THEN
      PERFORM log_event(  'info',
        concat(           'modify_attribute ',
                            object_id, ' ',
                            attribute, ' ',
                            modifier, ' ',
                            value ),
        concat(            'attempted to modify an attribute of a mod static ',
                            'class or a non existent object'  )
      );
      RETURN NULL;
    ELSIF avatar_is_connected IS NOT NULL
    AND   NOT avatar_is_connected
    THEN
      PERFORM log_event(  'info',
        concat(           'modify_attribute ',
                            object_id, ' ',
                            attribute, ' ',
                            modifier, ' ',
                            value ),
        concat(           'attempted to modify an attribute of a disconnected ',
                            'avatar'  )
      );
      RETURN NULL;
    ELSE
       /* Try to read the attribute old_value FROM the table, IF exists */
      old_value := get_attribute_value(
        object_id,
        attribute
      );

      /* Try to initialize it FROM inherited classes IF not found */
      IF old_value is NULL THEN
        old_value := initialize_attribute(
          object_id,
          attribute
        );
      END IF;

      /* If we are here and old_value is null, we give up */
      IF old_value IS NULL THEN
        PERFORM log_event(  'info',
          concat(           'modify_attribute ',
                              object_id, ' ',
                              attribute, ' ',
                              modifier, ' ',
                              value ),
          concat(           'attempted to modify an attribute that does not ',
                              'belong to the object'  )
        );
        RETURN NULL;
      ELSE
        IF    modifier = 'factor'
        THEN
          new_value := old_value * value;
        ELSIF modifier = 'set'
        OR    modifier = 'fix'
        THEN
          new_value := value;
        ELSE
          /* Default modifier is delta */
          new_value := old_value + value;
        END IF;

        /* Write the new attribute value in the database */
        --TODO: perhaps not the most efficient way to check this...
        IF old_value <> new_value THEN
          EXECUTE format(
            'UPDATE %I
             SET    %I = $1
             WHERE  id = %L',
            attr_record.in_table,
            attribute,
            object_id
          )
          USING new_value;

          /* Clean the garbage */
          DELETE FROM estimated_values;

          /* Check if we crossed some boundary */
          --TODO: also allow for 'continuous' sensory information
          --      e,g, temperature, in all its range
          SELECT *
          FROM resolve_boundaries(
            attribute,
            old_value,
            new_value
          )
          INTO boundaries_record;

          --TODO: leave this decision to listen_attribute()
          IF boundaries_record.direction IS NOT NULL THEN
            -- Process them
            --TODO: implement this using events rather than flags (less flexible but perhaps faster)
            input := jsonb_build_object(
              'caller'::text,     object_id::text,
              'target'::text,     array[object_id::text],
              'boundaries'::text, boundaries_record.boundaries::text[],
              'direction'::text,  array[boundaries_record.direction::text],
              'string'::TEXT,     string::TEXT
            );
             
            PERFORM log_event(  'debug',
              concat(           'modify_attribute ', jsonb_pretty(input) ),
                                'Crossed boundary, raising flag'
            );
/* TODO: generalize this call using a function that handles all the details
 * Do not raise a flag here, leave it to listen_attribute()
 *--------------------------------------------------------------------------
 * What we are doing here is trigger sensorial stimulus, so this calls for
 * some dedicated procedure for each sense:
 * - go fishing for guys with the relevant tags
 * - discriminate between connected avatars, disconnected ones, automata
 *   and populations
 * - either send messages or trigger responses, perhaps both in certain
 *   cases (death)
 *--------------------------------------------------------------------------
 */
            PERFORM raise_flag(
              'listens_'::VARCHAR || attribute::VARCHAR,
              input,
              TRUE
            );
          END IF;
        END IF;

        RETURN new_value;
      END IF;
    END IF;
  END;
$$ LANGUAGE plpgsql;

/* The attribute 'peaks' instantly, such as noise */
CREATE OR REPLACE FUNCTION make_instant_attribute(
  object_id VARCHAR,
  attribute VARCHAR,
  value     NUMERIC,
  string    VARCHAR   DEFAULT 'common'
)
RETURNS NUMERIC AS $$
  
  DECLARE
    boundaries_record   RECORD;
    input               jsonb;
  
  BEGIN
    /* Check if we crossed some boundary */
    --TODO: also allow for 'continuous' sensory information
    --      e,g, temperature, in all its range
    SELECT *
    FROM resolve_boundaries(
      attribute,
      0,
      value
    )
    INTO boundaries_record;

    --TODO: leave this decision to listen_attribute()
    IF boundaries_record.direction IS NOT NULL THEN
      -- Process them
      --TODO: implement this using events rather than flags (less flexible but perhaps faster)
      input := jsonb_build_object(
        'caller'::text,     object_id::text,
        'target'::text,     array[object_id::text],
        'boundaries'::text, boundaries_record.boundaries::text[],
        'direction'::text,  array[boundaries_record.direction::text],
        'string'::TEXT,     string::TEXT
      );
       
      PERFORM log_event(  'debug',
        concat(           'make_instant_attribute ', jsonb_pretty(input) ),
                          'Crossed boundary, raising flag'
      );

      PERFORM raise_flag(
        'listens_'::VARCHAR || attribute::VARCHAR,
        input,
        TRUE
      );
    END IF;

    RETURN value;
  END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION set_special_attribute(
  object_id VARCHAR,
  attribute VARCHAR,
  value     VARCHAR
)
RETURNS VARCHAR AS $$
  BEGIN
     IF EXISTS (
      SELECT
      FROM  links l
      WHERE l.id = object_id
      AND   l.target = object_id
      AND   l.type = attribute
    )
    THEN
      UPDATE  links
      SET     name = value
      WHERE   id = object_id
      AND     target = object_id
      AND     type = attribute;
    ELSE
       INSERT INTO links (
        id,
        target,
        type,
        name
      )
      VALUES (
        object_id,
        object_id,
        attribute,
        value
      );
    END IF;

     RETURN value;
  END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION get_special_attribute(
  object_id VARCHAR,
  attribute VARCHAR
)
RETURNS VARCHAR AS $$
  
  DECLARE
    value VARCHAR;
  
  BEGIN
     IF EXISTS (
      SELECT
      FROM  links l
      WHERE l.id = object_id
      AND   l.target = object_id
      AND   l.type = attribute
    )
    THEN
      SELECT  name
      FROM    links l
      WHERE   l.id = object_id
      AND     l.target = object_id
      AND     l.type = attribute
      INTO value;
       
      RETURN value;
    ELSE
      RETURN ''::VARCHAR;
    END IF;
  END;
$$ LANGUAGE plpgsql;


CREATE OR REPLACE FUNCTION skip_task(
  input jsonb
)
RETURNS INTEGER AS $$
  BEGIN
    PERFORM log_event(  'debug',
      concat(           'skip_task ', jsonb_pretty(input) ),
                        'Running dummy task FOR this trigger');
    RETURN 0;
  END;
$$ LANGUAGE plpgsql;

/*TODO: MIGRATE */
CREATE OR REPLACE FUNCTION initialize_knowledge(
  caller    VARCHAR,
  knowledge VARCHAR
)
RETURNS VARCHAR AS $$
  
  DECLARE
    learning      VARCHAR[];
    val           NUMERIC;
    json          TEXT;          
    knowledge_id  VARCHAR;
    learning_id   VARCHAR;

  BEGIN
    SELECT  l.id
    FROM    links   l,
            objects o
    WHERE   l.target = caller
    AND     l.id = o.id
    AND     knowledge = any(o.classes)
    AND     l.name = 'knowledge'
    INTO    knowledge_id;

    SELECT  l.id
    FROM    links   l,
            objects o
    WHERE   l.target = caller
    AND     l.id = o.id
    AND     knowledge = any(o.classes)
    AND     l.name = 'learning'
    INTO    learning_id;

    IF knowledge_id IS NOT NULL
    THEN
      RETURN NULL;
    ELSIF learning_id IS NOT NULL
    THEN
      RETURN learning_id;
    ELSE
      json := '{' ||
        '"class":' || to_json(knowledge) ||
        ',"parent":' || to_json(caller) ||
        ',"slot_name":"learning"'
      '}';

      learning_id := register_new_object(
        json::jsonb,
        TRUE
      );
      RETURN learning_id;
    END IF;
  END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION try_object(
  input jsonb
)
RETURNS INTEGER AS $$
  
  DECLARE
    roll          INTEGER[];
    dexterity     NUMERIC;
    intelligence  NUMERIC;
    curiosity     NUMERIC;
    wisdom        NUMERIC;
    coefficient   NUMERIC;
    k             VARCHAR;
    d             INTEGER;
    score         NUMERIC;
    val           NUMERIC;
    learning_id         VARCHAR;
    m             VARCHAR;
    json          TEXT;
    success       BOOLEAN;
    label         TEXT;

  BEGIN
    PERFORM log_event(  'debug',
      concat(           'try_object ', jsonb_pretty(input) ),
                        'Not implemented yet');

   /* TODO: implement some generic function that resolves
    * these (and others) from the json
    * TODO: think deeper into this coefficients thing
    */
    dexterity := estimate_attribute(input->>'caller'::VARCHAR, 'dexterity'::VARCHAR) / 80.0;
    intelligence := estimate_attribute(input->>'caller'::VARCHAR, 'intelligence'::VARCHAR) / 90.0;
    curiosity := estimate_attribute(input->>'caller'::VARCHAR, 'curiosity'::VARCHAR) / 60.0;
    wisdom := estimate_attribute(input->>'caller'::VARCHAR, 'wisdom'::VARCHAR) / 150.0;
    coefficient := (dexterity+intelligence)/2 + (curiosity+wisdom)/3;
    --visibility := estimate_attribute(input->>'caller'::VARCHAR, 'visibility'::VARCHAR);
    
    /* Cycle the knowledge objects */
    success := FALSE;
    FOR k IN
      SELECT *
      FROM   jsonb_array_elements_text( input->'knowledge_acquired' )
    LOOP
      roll := roll_dices_multi(input);
      learning_id := initialize_knowledge(input->>'caller', k);
 
      IF learning_id IS NOT NULL
      THEN
        FOREACH d IN ARRAY roll
        LOOP
          val := round(d * coefficient, 2);
          perform modify_attribute(
            learning_id,
            'learning_points',
            'delta',
            val::INTEGER,
            k
          );
          success := TRUE;
        END LOOP;
      END IF;

    END LOOP;

    /* Process phenomena */
    PERFORM make_phenomena(input);

    IF success
    THEN
      label := 'you_try';
    ELSE
      label := 'nothing_special';
    END IF;

    input := input || jsonb_build_object(
      'message_class'::TEXT,  'plain'::TEXT,
      'message_label'::TEXT,  label::TEXT,
      'subject'::TEXT,        input->'target',
      'recipient'::TEXT,      input->'caller',
      'target'::TEXT,         array[input->>'caller'::TEXT]
    );
    PERFORM push_api_call(
      'test_deliver',
      input
    );
    RETURN 0;
  END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION look_at(
  input jsonb
)
RETURNS INTEGER AS $$
  
  DECLARE
    nothing BOOLEAN;
    string  VARCHAR;
    o       VARCHAR;
    buf jsonb;

  BEGIN
    perform log_event('debug', 'Running task look_at', jsonb_pretty(input));
    nothing := TRUE;

    IF input ? 'parent'
    AND jsonb_array_length(input->'parent') != 0
    THEN
      nothing := FALSE;
      input := input || jsonb_build_object(
        'message_class'::TEXT,  'plain'::TEXT,
        'message_label'::TEXT,  'you_are_in'::TEXT,
        'subject'::TEXT,        input->'parent',
        'recipient'::TEXT,      input->'caller',
        'target'::TEXT,         array[input->>'caller'::TEXT]
      );

      PERFORM push_api_call(
        'test_deliver',
        input
      );
    END IF;

    IF input ? 'neighbours'
    AND jsonb_array_length(input->'neighbours') != 0
    THEN
      nothing := FALSE;
      input := input || jsonb_build_object(
        'message_class'::TEXT,  'plain'::TEXT,
        'message_label'::TEXT,  'your_neighbours'::TEXT,
        'subject'::TEXT,        input->'neighbours',
        'recipient'::TEXT,      input->'caller',
        'target'::TEXT,         array[input->>'caller'::TEXT]
      );

      PERFORM push_api_call(
        'test_deliver',
        input
      );
    END IF;

    IF input ? 'nearby'
    AND jsonb_array_length(input->'nearby') != 0
    THEN
      nothing := FALSE;
      input := input || jsonb_build_object(
        'message_class'::TEXT,  'plain'::TEXT,
        'message_label'::TEXT,  'you_see_nearby'::TEXT,
        'subject'::TEXT,        input->'nearby',
        'recipient'::TEXT,      input->'caller',
        'target'::TEXT,         array[input->>'caller'::TEXT]
      );

      PERFORM push_api_call(
        'test_deliver',
        input
      );
    END IF;

    IF input ? 'analyze'
    AND jsonb_array_length(input->'analyze') != 0
    THEN
      nothing := FALSE;
      input := input || jsonb_build_object(
        'message_class'::TEXT,  'plain'::TEXT,
        'message_label'::TEXT,  'analyze'::TEXT,
        'subject'::TEXT,        input->'analyze',
        'recipient'::TEXT,      input->'caller',
        'target'::TEXT,         array[input->>'caller'::TEXT]
      );

      PERFORM push_api_call(
        'test_deliver',
        input
      );
    END IF;

    IF input ? 'inventory'
    AND jsonb_array_length(input->'inventory') != 0
    THEN
      nothing := FALSE;
      input := input || jsonb_build_object(
        'message_class'::TEXT,  'plain'::TEXT,
        'message_label'::TEXT,  'you_have'::TEXT,
        'subject'::TEXT,        input->'inventory',
        'recipient'::TEXT,      input->'caller',
        'target'::TEXT,         array[input->>'caller'::TEXT]
      );

      PERFORM push_api_call(
        'test_deliver',
        input
      );
    END IF;

    IF input ? 'in_use'
    AND jsonb_array_length(input->'in_use') != 0
    THEN
      nothing := FALSE;
      for o in select * from jsonb_array_elements_text(input->'in_use')
      loop
        select name from links where id = input->>'caller' and target = o and type = 'use'
        into string;

        input := input || jsonb_build_object(
          'message_class'::TEXT,  'plain'::TEXT,
          'message_label'::TEXT,  concat('you_use_as_'::TEXT, string::TEXT),
          'subject'::TEXT,        array[o::TEXT],
          'recipient'::TEXT,      input->'caller',
          'target'::TEXT,         array[input->>'caller'::TEXT]
        );

        PERFORM push_api_call(
          'test_deliver',
          input
        );
      end loop;
    END IF;

    IF nothing IS TRUE
    THEN
      input := input || jsonb_build_object(
        'message_class'::TEXT,  'plain'::TEXT,
        'message_label'::TEXT,  'nothing_to_see'::TEXT,
        'recipient'::TEXT,      input->'caller',
        'target'::TEXT,         array[input->>'caller'::TEXT]
      );

      PERFORM push_api_call(
        'test_deliver',
        input
      );
    END IF;

    RETURN 0;
  END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION show_available_directions(
  input jsonb
)
RETURNS INTEGER AS $$
  
  DECLARE
    nothing BOOLEAN;
    string  VARCHAR;
    o       VARCHAR;
    buf jsonb;

  BEGIN
    perform log_event('debug', 'Running task show_available_directions', jsonb_pretty(input));
    nothing := TRUE;

    IF input ? 'neighbours'
    AND jsonb_array_length(input->'neighbours') != 0
    THEN
      nothing := FALSE;
      input := input || jsonb_build_object(
        'message_class'::TEXT,  'plain'::TEXT,
        'message_label'::TEXT,  'you_can_travel_to'::TEXT,
        'subject'::TEXT,        input->'neighbours',
        'recipient'::TEXT,      input->'caller',
        'target'::TEXT,         array[input->>'caller'::TEXT]
      );

      PERFORM push_api_call(
        'test_deliver',
        input
      );
    END IF;

    IF input ? 'connected_to_parent'
    AND jsonb_array_length(input->'connected_to_parent') != 0
    THEN
      nothing := FALSE;
      input := input || jsonb_build_object(
        'message_class'::TEXT,  'plain'::TEXT,
        'message_label'::TEXT,  'you_can_enter'::TEXT,
        'subject'::TEXT,        input->'connected_to_parent',
        'recipient'::TEXT,      input->'caller',
        'target'::TEXT,         array[input->>'caller'::TEXT]
      );

      PERFORM push_api_call(
        'test_deliver',
        input
      );
    END IF;

    IF input ? 'reachable_slots'
    AND jsonb_array_length(input->'reachable_slots') != 0
    THEN
      nothing := FALSE;
      input := input || jsonb_build_object(
        'message_class'::TEXT,  'plain'::TEXT,
        'message_label'::TEXT,  'you_can_go_to'::TEXT,
        'subject'::TEXT,        input->'reachable_slots',
        'recipient'::TEXT,      input->'caller',
        'target'::TEXT,         array[input->>'caller'::TEXT]
      );

      PERFORM push_api_call(
        'test_deliver',
        input
      );
    END IF;

    IF input ? 'grandparent'
    AND jsonb_array_length(input->'grandparent') != 0
    THEN
      nothing := FALSE;
      input := input || jsonb_build_object(
        'message_class'::TEXT,  'plain'::TEXT,
        'message_label'::TEXT,  'you_can_leave'::TEXT,
        'subject'::TEXT,        input->'grandparent',
        'recipient'::TEXT,      input->'caller',
        'target'::TEXT,         array[input->>'caller'::TEXT]
      );

      PERFORM push_api_call(
        'test_deliver',
        input
      );
    END IF;

    IF nothing IS TRUE
    THEN
      input := input || jsonb_build_object(
        'message_class'::TEXT,  'plain'::TEXT,
        'message_label'::TEXT,  'nowhere_to_go'::TEXT,
        'recipient'::TEXT,      input->'caller',
        'target'::TEXT,         array[input->>'caller'::TEXT]
      );

      PERFORM push_api_call(
        'test_deliver',
        input
      );
    END IF;

    RETURN 0;
  END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION use_object(
  input jsonb
)
RETURNS INTEGER AS $$
  
  DECLARE
    use_as    VARCHAR;
    t         VARCHAR;
    success   BOOLEAN;
    buf jsonb;

  BEGIN
    IF NOT input ? 'caller'
    OR NOT input ? 'target'
    THEN
      /* Input must have keys 'caller' AND 'class' */
      PERFORM log_event(  'info',
        concat(           'use_object ', jsonb_pretty(input) ),
                          'No target or caller provided'
      );
      RAISE EXCEPTION     'No target or caller provided';
      RETURN ''::VARCHAR;
    ELSE

      success := false;
      use_as := input->>'use_as';
  
      FOR t IN
        SELECT *
        FROM   jsonb_array_elements_text( input->'use' )
      LOOP
        if not exists (
          select * from get_all_tags(t) tag where tag = concat('use_as_', use_as)
        ) then
          buf := input || jsonb_build_object(
            'message_class'::TEXT,  'plain'::TEXT,
            'message_label'::TEXT,  'not_usable'::TEXT,
            'subject'::TEXT,        t::TEXT,
            'recipient'::TEXT,      input->'caller',
            'target'::TEXT,         array[input->>'caller'::TEXT]
          );
  
          PERFORM push_api_call(
            'test_deliver',
            buf
          );
        elsif exists (
          select id from links where target = t and type = 'use' and id = input->>'caller'
        ) then
          update links set name = use_as where type = 'use' and target = t and id = input->>'caller';
          success := true;
        else
          insert into links (id, target, type, name) values (input->>'caller', t, 'use', use_as);
          success := true;
        end if;
      END LOOP;

      IF success THEN
        input := input || jsonb_build_object(
          'message_class'::TEXT,  'plain'::TEXT,
          'message_label'::TEXT,  'using_as'::TEXT,
          'subject'::TEXT,        input->'target',
          'recipient'::TEXT,      input->'caller',
          'target'::TEXT,         array[input->>'caller'::TEXT]
        );
      ELSE
        input := input || jsonb_build_object(
          'message_class'::TEXT,  'plain'::TEXT,
          'message_label'::TEXT,  'nothing_to_use'::TEXT,
          'subject'::TEXT,        input->'target',
          'recipient'::TEXT,      input->'caller',
          'target'::TEXT,         array[input->>'caller'::TEXT]
        );
      END IF;
      PERFORM push_api_call(
        'test_deliver',
        input
      );
  
    END IF;

    RETURN 0;
  END;
$$ LANGUAGE plpgsql;
  
CREATE OR REPLACE FUNCTION build_object(
  input jsonb
)
RETURNS INTEGER AS $$
  
  DECLARE
    p         VARCHAR;
    m         VARCHAR;
    c         VARCHAR;
    t         VARCHAR;
    new_id    VARCHAR;
    json      TEXT;
    i         NUMERIC;
  
  BEGIN
    IF NOT input ? 'caller'
    OR NOT input ? 'target'
    THEN
      /* Input must have keys 'caller' AND 'class' */
      PERFORM log_event(  'info',
        concat(           'prepare_parent_link ', jsonb_pretty(input) ),
                          'No target or caller provided'
      );
      RAISE EXCEPTION     'No target or caller provided';
      RETURN ''::VARCHAR;
    ELSE
      IF  input ? 'in_parent'
      THEN
        select input->'in_parent'->>0
        INTO p;
      ELSE
        /* The new object will be created in the caller's parent */
        SELECT target
        FROM   links
        WHERE  type = 'parent'
        AND    id = input->>'caller'
        INTO   p;
      END IF;
      /* Create one object for each target*/
      IF input ? 'result'
      THEN
        FOR t IN
          SELECT *
          FROM   json_array_elements( to_json(input->'result') )
        LOOP
          json := '{' ||
            '"class":' || t ||
            ',"parent":' || to_json(p) ||
            ',"init":"yes"' ||
          '}';
  
          PERFORM register_new_object(
            json::jsonb,
            TRUE
          );
        END LOOP;
      ELSE
        FOR t IN
          SELECT *
          FROM   json_array_elements( to_json(input->'target') )
        LOOP
          json := '{' ||
            '"class":' || t ||
            ',"parent":' || to_json(p) ||
            ',"init":"yes"' ||
          '}';
  
          new_id := register_new_object(
            json::jsonb,
            TRUE
          );
        END LOOP;
      END IF;

      /* Link the components */
      FOR c IN
        SELECT *
        FROM   jsonb_array_elements_text( input->'components' )
      LOOP
        DELETE
        FROM    links l
        WHERE   l.id = c
        AND     l.type = 'parent';

        UPDATE  objects
        SET     active = FALSE
        WHERE   id = c;

        INSERT INTO links (
          id,
          type,
          name,
          target 
        )
        VALUES (
          new_id,
          'component',
          'material',
          c
        );
      END LOOP;
        
      /* Process phenomena */
      PERFORM make_phenomena(input);

      /* Prepare and send message */
      /* TODO: this should be someone other's job */
      input := input || jsonb_build_object(
        'message_class'::TEXT,  'plain'::TEXT,
        'message_label'::TEXT,  'you_have_built'::TEXT,
        'subject'::TEXT,        input->'target',
        'recipient'::TEXT,      input->'caller',
        'target'::TEXT,         array[input->>'caller'::TEXT]
      );

      PERFORM push_api_call(
        'test_deliver',
        input
      );
      RETURN 0;
    END IF;   
  END;
$$ LANGUAGE plpgsql;

/*TODO: MIGRATE */
CREATE OR REPLACE FUNCTION roll_dice(
  faces INTEGER
)
RETURNS INTEGER AS $$
  BEGIN
    RETURN
      round( faces*random() + 1 )::INTEGER;
  END;
$$ LANGUAGE plpgsql;

/*TODO: MIGRATE */
CREATE OR REPLACE FUNCTION roll_dices(
  number_of_dices INTEGER,
  faces           INTEGER
)
RETURNS INTEGER AS $$

  DECLARE
    i       INTEGER;
    result  INTEGER;

  BEGIN
    i := 0;
    result := 0;

    LOOP
      i := i + 1;
      result := result + roll_dice(faces);
      IF i >= number_of_dices
      THEN
        EXIT;
      END IF;
    END LOOP;
    
    RETURN result;
  END;
$$ LANGUAGE plpgsql;

/*TODO: MIGRATE */
CREATE OR REPLACE FUNCTION roll_dices_multi(
  input jsonb
)
RETURNS INTEGER[] AS $$
  
  DECLARE
    throws      INTEGER;
    dices       INTEGER;
    faces       INTEGER;
    i           INTEGER;
    t           INTEGER;
    this_throw  INTEGER;
    result      INTEGER[];
  
  BEGIN
    throws := jsonb_array_elements( input->'dice_throws' )::INTEGER;
    dices  := jsonb_array_elements( input->'number_of_dices' )::INTEGER;
    faces  := jsonb_array_elements( input->'dice_faces' )::INTEGER;

    t := 0;
    LOOP
      t := t + 1;
      result := result || array[ roll_dices(dices, faces) ];
      IF t >= throws
      THEN
        EXIT;
      END IF;
    END LOOP;

    RETURN result;
  END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION extract_resource(
  input jsonb
)
RETURNS INTEGER AS $$
  
  DECLARE
    p             VARCHAR;
    m             VARCHAR;
    s             VARCHAR;
    r             VARCHAR;
    i             NUMERIC;
    json          TEXT;
    action        VARCHAR;
    trigg         RECORD;
    this_buffer   JSONB;
    roll          INTEGER[];
    dexterity     NUMERIC;
    visibility    NUMERIC;
    d             INTEGER;
    c             VARCHAR;
    result        VARCHAR[];
    material      VARCHAR;
    foo RECORD;
    val NUMERIC;
    msglabel      VARCHAR;
  
  BEGIN
    IF NOT input ? 'caller'
    OR NOT input ? 'target'
    THEN
      /* Input must have keys 'caller' AND 'class' */
      PERFORM log_event(  'info',
        concat(           'prepare_parent_link ', jsonb_pretty(input) ),
                          'No target or caller provided'
      );
      RAISE EXCEPTION     'No target or caller provided';
      RETURN ''::VARCHAR;
    ELSE
      /* The new objects will be created in the caller's parent */
      /* TODO: water should be one exception, among others      */
      /* Solution: pick it from the trigger                     */
      SELECT target
      FROM   links
      WHERE  type = 'parent'
      AND    id = input->>'caller'
      INTO   p;
      
      action := concat(
        jsonb_array_elements_text(input->'action'),
        '_',
        jsonb_array_elements_text(input->'target')
      );
      CREATE TEMP TABLE candidates (
        id        VARCHAR,
        scarcity  NUMERIC
      ) ON COMMIT DROP;
      /* Cycle through the sources */
      FOREACH s IN
        ARRAY array[ jsonb_array_elements_text( input->'source' ) ]
      LOOP
        FOR trigg IN
          SELECT  *
          FROM    triggers t
          WHERE   t.target = any(
            array( SELECT get_all_inherited(s) )
          )
          AND     action = any(t.tags)
        LOOP
          SELECT run_trigger
          FROM   run_trigger(
            trigg.id,
            input
          )
          INTO   this_buffer;
          FOR c IN SELECT jsonb_array_elements_text( this_buffer->'candidates' )
          LOOP
            INSERT INTO candidates (id, scarcity) VALUES (
              c,
              get_attribute_value(c, 'resource_scarcity'::VARCHAR)
            );
          END LOOP;

        roll := roll_dices_multi(this_buffer);
       /* TODO: implement some generic function that resolves
        * these (and others) from the json
        */
        dexterity := estimate_attribute(input->>'caller'::VARCHAR, 'dexterity'::VARCHAR) / 110.0;
        visibility := estimate_attribute(input->>'caller'::VARCHAR, 'visibility'::VARCHAR);

        FOREACH d IN ARRAY roll
        LOOP
          for foo in select id, scarcity from candidates order by scarcity desc loop
            val := d * dexterity;
            if val >= foo.scarcity
            then
              result := result || array[ foo.id ];
              json := '{' ||
                '"class":' || to_json(foo.id) ||
                ',"parent":' || to_json(p) ||
                ',"init":"yes"' ||
              '}';
      
              PERFORM register_new_object(
                json::jsonb,
                TRUE
              );
              exit;
            end if;
          end loop;
        END LOOP;
        END LOOP;
      END LOOP;

      /* Process phenomena */
      PERFORM make_phenomena(input);

      /* Prepare and send message */
      /* TODO: this should be someone other's job */
      IF array_upper(result, 1) IS NULL
      THEN
        msglabel := 'you_collected_nothing';
      ELSE
        msglabel := 'you_have_collected';
      END IF;
      input := input || jsonb_build_object(
        'result'::TEXT,         result::text[],
        'message_class'::TEXT,  'plain'::TEXT,
        'message_label'::TEXT,  msglabel::TEXT,
        'subject'::TEXT,        input->'target',
        'recipient'::TEXT,      input->'caller',
        'target'::TEXT,         array[input->>'caller'::TEXT]
      );

      PERFORM push_api_call(
        'test_deliver',
        input
      );
      
      drop table candidates;

      RETURN 0;
    END IF;   
   END;
$$ LANGUAGE plpgsql;

/* TODO: migrate this to gaia */
CREATE OR REPLACE FUNCTION create_object(
  input jsonb
)
RETURNS INTEGER AS $$
  
  DECLARE
    p     VARCHAR;
    m     VARCHAR;
    t     VARCHAR;
    json  TEXT;
    i     NUMERIC;
  
  BEGIN
    IF NOT input ? 'caller'
    OR NOT input ? 'target'
    THEN
      /* Input must have keys 'caller' AND 'class' */
      PERFORM log_event(  'info',
        concat(           'prepare_parent_link ', jsonb_pretty(input) ),
                          'No target or caller provided'
      );
      RAISE EXCEPTION     'No target or caller provided';
      RETURN ''::VARCHAR;
    ELSE
      /* The new object will be created in the caller's parent */
      SELECT target
      FROM   links
      WHERE  type = 'parent'
      AND    id = input->>'caller'
      INTO   p;
      /* Create one object for each target*/
      FOR t IN
        SELECT *
        FROM   json_array_elements( to_json(input->'target') )
      LOOP
        json := '{' ||
          '"class":' || t ||
          ',"parent":' || to_json(p) ||
          ',"init":"yes"' ||
        '}';

        PERFORM register_new_object(
          json::jsonb,
          TRUE
        );
      END LOOP;
      /* Prepare and send message */
      /* TODO: this should be someone other's job */
      input := input || jsonb_build_object(
        'message_class'::TEXT,  'plain'::TEXT,
        'message_label'::TEXT,  'you_have_created'::TEXT,
        'subject'::TEXT,        input->'target',
        'recipient'::TEXT,      input->'caller',
        'target'::TEXT,         array[input->>'caller'::TEXT]
      );

      PERFORM push_api_call(
        'test_deliver',
        input
      );
      RETURN 0;
    END IF;   
   END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION pick_object(
  input jsonb
)
RETURNS INTEGER AS $$
  
  DECLARE
    p         VARCHAR;
    m         VARCHAR;
    t         VARCHAR;
    json      TEXT;
    buffer    jsonb;
    placement RECORD;
  
  BEGIN
    IF NOT input ? 'caller'
    OR NOT input ? 'target'
    THEN
      /* Input must have keys 'caller' AND 'class' */
      PERFORM log_event(  'info',
        concat(           'pick_object ', jsonb_pretty(input) ),
                          'No target or caller provided'
      );
      RAISE EXCEPTION     'No target or caller provided';
      RETURN ''::VARCHAR;
    ELSE
      /* Do the job */
      FOR t IN
        SELECT *
        FROM   json_array_elements( to_json(input->'picked') )
      LOOP
        json := '{' ||
          '"child":' || t ||
          ',"parent":' || to_json(input->'caller'::VARCHAR) ||
        '}';
        
        SELECT *
        FROM   put_object_in_place(
          json::jsonb,
          TRUE
        )
        INTO placement;
      END LOOP;

      /* Prepare and send message */
      IF placement.success THEN
        input := input || jsonb_build_object(
          'message_class'::TEXT,  'plain'::TEXT,
          'message_label'::TEXT,  'you_have_picked'::TEXT,
          'subject'::TEXT,        input->'picked',
          'recipient'::TEXT,      input->'caller',
          'target'::TEXT,         array[input->>'caller'::TEXT]
        );
        
        PERFORM push_api_call(
          'test_deliver',
          input
        );

        /* Process phenomena */
        PERFORM make_phenomena(input);

      ELSE
        input := input || jsonb_build_object(
          'message_class'::TEXT,  'plain'::TEXT,
          'message_label'::TEXT,  'you_cannot_pick_that'::TEXT,
          'subject'::TEXT,        input->'picked',
          'recipient'::TEXT,      input->'caller',
          'target'::TEXT,         array[input->>'caller'::TEXT]
        );

        PERFORM push_api_call(
          'test_deliver',
          input
        );
      END IF;
      
      RETURN 0;
    END if;   
  END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION drop_object(
  input jsonb
)
RETURNS INTEGER AS $$
  
  DECLARE
    p         VARCHAR;
    m         VARCHAR;
    t         VARCHAR;
    json      TEXT;
    buffer    jsonb;
    placement RECORD;
  
  BEGIN
    IF NOT input ? 'caller'
    OR NOT input ? 'target'
    THEN
      /* Input must have keys 'caller' AND 'class' */
      PERFORM log_event(  'info',
        concat(           'prepare_parent_link ', jsonb_pretty(input) ),
                          'No target or caller provided'
      );
      RAISE EXCEPTION     'No target or caller provided';
      RETURN ''::VARCHAR;
    ELSE
      /* Do the job */
      SELECT target
      FROM   links
      WHERE  type = 'parent'
      AND    id = input->>'caller'
      INTO   p;

      FOR t IN
        SELECT *
        FROM   json_array_elements( to_json(input->'dropped') )
      LOOP
        json := '{' ||
          '"child":' || t ||
          ',"parent":' || to_json(p) ||
        '}';

        SELECT *
        FROM   put_object_in_place(
          json::jsonb,
          TRUE
        )
        INTO placement;
      END LOOP;

      /* Prepare and send message */
      /* TODO: this should be someone else's job */
      IF placement.success THEN
        input := input || jsonb_build_object(
          'message_class'::TEXT,  'plain'::TEXT,
          'message_label'::TEXT,  'you_have_dropped'::TEXT,
          'subject'::TEXT,        input->'dropped',
          'recipient'::TEXT,      input->'caller'::TEXT,
          'target'::TEXT,         array[input->>'caller'::TEXT]
        );

        PERFORM push_api_call(
          'test_deliver',
          input
        );

        /* Process phenomena */
        PERFORM make_phenomena(input);

      ELSE
        input := input || jsonb_build_object(
          'message_class'::TEXT,  'plain'::TEXT,
          'message_label'::TEXT,  'you_cannot_drop_that'::TEXT,
          'subject'::TEXT,        input->'dropped',
          'recipient'::TEXT,      input->'caller',
          'target'::TEXT,         array[input->>'caller'::TEXT]
        );

        PERFORM push_api_call(
          'test_deliver',
          input
        );
      END IF
      ;
      RETURN 0;
    END IF;   
  END;
$$ LANGUAGE plpgsql;

/*
 * Events
 */

/*TODO: MIGRATE */
CREATE OR REPLACE FUNCTION makes_noise(
  id    VARCHAR,
  value NUMERIC
)
RETURNS VOID AS $$
  BEGIN
    PERFORM log_event(  'debug',
      concat(           id::TEXT, ' makes_noise ', value::TEXT ),
                        'abracadabra!'
    );
    RETURN;
  END;
$$ LANGUAGE plpgsql;

